package uk.co.christopherpritchard.odeoncinemaprojectGUI;

import uk.co.christopherpritchard.odeoncinemaproject.*;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class MainUIPanel extends JPanel {
    private List<MainUIPanelListener> listeners;

    public MainUIPanel() {
        this.listeners = new ArrayList<>();
    }

    void addEventListener(MainUIPanelListener listener){
        listeners.add(listener);
    }

    void removeEventListener(MainUIPanelListener listener){
        listeners.remove(listener);
    }

    void mainUILoggedOut(){
        listeners.forEach(MainUIPanelListener::mainUIPanelLoggedOut);
    }

    void customerUINewBooking(){
        listeners.forEach(MainUIPanelListener::newBooking);
    }

    void customerUIAddModifyReview(Film film){
        listeners.forEach(o->o.customerUIAddModifyReview(film));
    }

    void customerUIAddModifyReview(Showing showing){
        listeners.forEach(o->o.customerUIAddModifyReview(showing.getFilm()));
    }

    void modifyCustomer(){
        listeners.forEach(MainUIPanelListener::modifyCustomer);
    }

    void modifyManager(){
        listeners.forEach(MainUIPanelListener::modifyManager);
    }

    void newBooking(Customer customer){
        listeners.forEach(o->o.newBooking(customer));
    }
    void uiBack(JPanel panel){
        listeners.forEach(o->o.uiBack(panel));
    }

    void customerUIFutureBookings(){
        listeners.forEach(MainUIPanelListener::futureBookings);
    }

    void customerUIPastBookings(){
        listeners.forEach(MainUIPanelListener::pastBookings);
    }

    void futureBookings(Customer cust){
        listeners.forEach(o->o.futureBookings(cust));
    }

    void pastBookings(Customer cust){
        listeners.forEach(o->o.pastBookings(cust));
    }

    void modifyBooking(Booking booking, JPanel panel, Customer customer){
        listeners.forEach(o->o.modifyBooking(booking, panel, customer));
    }

    void filmReview(Film film){
        listeners.forEach(o->o.filmReview(film));
    }

    void addFilm(){
        listeners.forEach(MainUIPanelListener::addFilm);
    }

    void addShowing(){
        listeners.forEach(MainUIPanelListener::addShowing);
    }

    void addShowing(Film film){
        listeners.forEach(o->o.addShowing(film));
    }

    void addShowing(Screen screen){
        listeners.forEach(o->o.addShowing(screen));
    }

    void addManager(){listeners.forEach(o->o.addManager());}
}
