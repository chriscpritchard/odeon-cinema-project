package uk.co.christopherpritchard.odeoncinemaprojectGUI;

import uk.co.christopherpritchard.odeoncinemaproject.*;

import javax.naming.NoPermissionException;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

class NewManagerUI extends NewUserUI {

    private CinemaBookingSystem cinema;
    private List<NewUserUIListener> listeners;
    private User modifyUser;

    public NewManagerUI(CinemaBookingSystem cinema) {
        super(cinema);
        super.addButtonListener(new NewManagerButtonListener());
        this.cinema = cinema;
        this.listeners = new ArrayList<>();
        super.setTitle("New Manager - Odeon Cinema System");
        super.setVisible(true);
    }

    public NewManagerUI(CinemaBookingSystem cinema, Manager manager) {
        super(cinema, manager);
        this.modifyUser = manager;
        super.addButtonListener(new NewManagerUI.ChangeManagerButtonListener());
        this.cinema = cinema;
        this.listeners = new ArrayList<>();
        super.setTitle("Change Details - Odeon Cinema System");
        super.setVisible(true);
    }

    class ChangeManagerButtonListener extends CreateListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                if (modifyUser.authenticate(NewManagerUI.super.getExistingPassword())){
                    if (NewManagerUI.super.checkPasswordFields()){
                        if (NewManagerUI.this.getPasswordText().length == 0){
                            cinema.changeUser(modifyUser, NewManagerUI.this.getUsernameText(), NewManagerUI.this.getFirstNameText(), NewManagerUI.this.getLastNameText(), NewManagerUI.super.getExistingPassword());
                            listeners.forEach(NewUserUIListener::userModified);
                            NewManagerUI.super.dispose();
                        } else {
                            cinema.changeUser(modifyUser, NewManagerUI.this.getUsernameText(), NewManagerUI.this.getFirstNameText(), NewManagerUI.this.getLastNameText(),NewManagerUI.super.getPasswordText(), NewManagerUI.super.getExistingPassword());
                            listeners.forEach(NewUserUIListener::userModified);
                            NewManagerUI.super.dispose();
                        }
                    } else {
                        throw new UserAddFailedException("The passwords do not match");
                    }
                    NewManagerUI.super.clearPassword();
                }else{
                    throw new UserAddFailedException("Please enter existing password");
                }
            }catch (InvalidLoginException | UserAddFailedException ex){
                JOptionPane.showMessageDialog(NewManagerUI.this, ex.getMessage(), "Error modifying user", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    @Override
    void addEventListener(NewUserUIListener listener) {
        this.listeners.add(listener);
    }

    class NewManagerButtonListener extends CreateListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            String username = NewManagerUI.super.getUsernameText();
            try{
                if(NewManagerUI.super.checkPasswordFields()){
                    cinema.addManager(username, NewManagerUI.super.getFirstNameText(), NewManagerUI.super.getLastNameText(), NewManagerUI.super.getPasswordText());
                    JOptionPane.showMessageDialog(NewManagerUI.this, "Successfully created user: " + username, "User created", JOptionPane.INFORMATION_MESSAGE);
                    listeners.forEach(o->o.newUserCreated(username));
                    NewManagerUI.super.dispose();
                }else {
                    throw new UserAddFailedException("The passwords do not match");
                }
            } catch (UserAddFailedException | NoPermissionException ex) {
                NewManagerUI.super.clearPassword();
                JOptionPane.showMessageDialog(NewManagerUI.this, ex.getMessage(), "Error adding user", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        listeners.forEach(NewUserUIListener::newUserUIclosed);
    }
}
