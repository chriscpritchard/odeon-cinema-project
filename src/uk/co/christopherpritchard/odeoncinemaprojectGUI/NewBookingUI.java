package uk.co.christopherpritchard.odeoncinemaprojectGUI;

import com.github.lgooddatepicker.components.DatePicker;
import com.github.lgooddatepicker.optionalusertools.DateChangeListener;
import com.github.lgooddatepicker.zinternaltools.DateChangeEvent;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import uk.co.christopherpritchard.CollectionTools.CollectionToCollection;
import uk.co.christopherpritchard.odeoncinemaproject.*;

import javax.naming.NoPermissionException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.time.LocalDate;
import java.util.Comparator;

public class NewBookingUI {
    private JPanel newBookingUIPanel;
    private MainUIPanel thisPanel;
    private JButton addBookingButton;
    private JComboBox<Film> filmList;
    private JComboBox<Showing> showList;
    private JComboBox<String> paymentComboBox;
    private JTextField screenTextField;
    private JButton backButton;
    private DatePicker showDate;
    private JTextField costTextField;
    private JComboBox<String> seatTypeCombo;
    private JTextField seatsRemainingTextField;
    private JLabel newBookingLabel;
    private CinemaBookingSystem bookingSystem;
    private Film film;
    private Showing showing;
    private Customer cust;

    public NewBookingUI(CinemaBookingSystem bookingSystem) {
        this(bookingSystem, (Customer) bookingSystem.getAuthenticatedUser());
    }

    public NewBookingUI(CinemaBookingSystem bookingSystem, Booking existingBooking) {
        this(bookingSystem, existingBooking, (Customer) bookingSystem.getAuthenticatedUser());
    }

    public NewBookingUI(CinemaBookingSystem bookingSystem, Customer cust) {
        super();
        this.cust = cust;
        this.bookingSystem = bookingSystem;
        //Automatically inserted by UI generator!
        $$$setupUI$$$();
        showDate.setDateToToday();
        initialiseLists();
        initialiseListeners();
        newBookingLabel.setText("New Booking - " + cust.toString());
        addBookingButton.addActionListener(new ActionListener() {
            //void addBooking(Showing showing, String seatType, Customer customer, Payment payment) throws BookingFailedException, NoPermissionException;
            @Override
            public void actionPerformed(ActionEvent e) {
                if (paymentComboBox.getSelectedItem() != null && seatTypeCombo.getSelectedItem() != null) {
                    try {
                        switch (paymentComboBox.getSelectedItem().toString()) {
                            case "Cash":
                                bookingSystem.addBooking(showing, seatTypeCombo.getSelectedItem().toString(), cust, new CashPayment(bookingSystem.getPrice(showing, seatTypeCombo.getSelectedItem().toString())));
                                break;
                            case "Card":
                                bookingSystem.addBooking(showing, seatTypeCombo.getSelectedItem().toString(), cust, new CardPayment(bookingSystem.getPrice(showing, seatTypeCombo.getSelectedItem().toString())));
                                JOptionPane.showMessageDialog(NewBookingUI.this.$$$getRootComponent$$$(), "This is where a card payment dialog could be shown", "Card Payment", JOptionPane.INFORMATION_MESSAGE);
                        }
                        JOptionPane.showMessageDialog(NewBookingUI.this.$$$getRootComponent$$$(), "Booking added successfully", "Booking complete", JOptionPane.INFORMATION_MESSAGE);
                        refreshTextFields();
                        showDate.setDateToToday();
                        thisPanel.uiBack(thisPanel);
                    } catch (BookingFailedException | NoPermissionException | IllegalArgumentException ex) {
                        JOptionPane.showMessageDialog(NewBookingUI.this.$$$getRootComponent$$$(), ex.getMessage(), "Error adding booking", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });
    }

    public NewBookingUI(CinemaBookingSystem bookingSystem, Booking existingBooking, Customer cust) {
        super();
        this.cust = cust;
        this.bookingSystem = bookingSystem;
        //Automatically inserted by UI generator!
        $$$setupUI$$$();
        this.showDate.setDate(existingBooking.getShowing().getDatetime().toLocalDate());
        this.showDate.getSettings().setAllowEmptyDates(false);
        this.showDate.getSettings().setDateRangeLimits(LocalDate.now(), null);
        initialiseLists();
        this.filmList.setSelectedItem(existingBooking.getShowing().getFilm());
        refreshShowingList();
        this.showList.setSelectedItem(existingBooking.getShowing());
        this.paymentComboBox.setSelectedItem(existingBooking.getPayment().getType());
        this.seatTypeCombo.setSelectedItem(existingBooking.getSeat().getSeatType());
        this.newBookingLabel.setText("Modify Booking: \n" + existingBooking.toString());
        this.addBookingButton.setText("Modify Booking");
        refreshTextFields();
        initialiseListeners();
        addBookingButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if (paymentComboBox.getSelectedItem() != null && seatTypeCombo.getSelectedItem() != null) {
                        switch (paymentComboBox.getSelectedItem().toString()) {
                            case "Cash":
                                bookingSystem.addBooking(showing, seatTypeCombo.getSelectedItem().toString(), cust, new CashPayment(bookingSystem.getPrice(showing, seatTypeCombo.getSelectedItem().toString())));
                                bookingSystem.removeBooking(existingBooking);
                                break;
                            case "Card":
                                bookingSystem.addBooking(showing, seatTypeCombo.getSelectedItem().toString(), cust, new CardPayment(bookingSystem.getPrice(showing, seatTypeCombo.getSelectedItem().toString())));
                                JOptionPane.showMessageDialog(NewBookingUI.this.getPanel(), "This is where a card payment dialog could be shown", "Card Payment", JOptionPane.INFORMATION_MESSAGE);
                                bookingSystem.removeBooking(existingBooking);
                        }
                        JOptionPane.showMessageDialog(NewBookingUI.this.getPanel(), "Booking changed successfully", "Booking complete", JOptionPane.INFORMATION_MESSAGE);
                        refreshTextFields();
                        thisPanel.uiBack(thisPanel);
                    }
                } catch (BookingFailedException | NoPermissionException | IllegalArgumentException ex) {
                    JOptionPane.showMessageDialog(NewBookingUI.this.getPanel(), ex.getMessage(), "Error adding booking", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }

    private void createUIComponents() {
        newBookingUIPanel = new MainUIPanel();
        showDate = new DatePicker();
        filmList = new JComboBox<>();
        showList = new JComboBox<>();
        paymentComboBox = new JComboBox<>();
        seatTypeCombo = new JComboBox<>();
    }

    public MainUIPanel getPanel() {
        return thisPanel;
    }

    private void setStaticComboBoxes() {
        seatTypeCombo.addItem("Standard");
        seatTypeCombo.addItem("VIP");
        paymentComboBox.addItem("Card");
        paymentComboBox.addItem("Cash");
    }


    private void refreshFilmList() {
        filmList.removeAllItems();
        java.util.List<Film> films = new CollectionToCollection<>(bookingSystem.getFilms(showDate.getDate())).getSortedList(new Comparator<Film>() {
            @Override
            public int compare(Film o1, Film o2) {
                return o1.getName().compareToIgnoreCase(o2.getName());
            }
        });
        for (Film film : films) {
            if (((DefaultComboBoxModel) filmList.getModel()).getIndexOf(film) == -1) {
                filmList.addItem(film);
            }
        }
    }

    private void refreshShowingList() {
        showList.removeAllItems();
        if (filmList.getSelectedItem() != null) {
            java.util.List<Showing> showings = new CollectionToCollection<>(bookingSystem.getShowings(filmList.getSelectedItem().toString(), showDate.getDate())).getSortedList(new Comparator<Showing>() {
                @Override
                public int compare(Showing o1, Showing o2) {
                    return o1.getDatetime().compareTo(o2.getDatetime());
                }
            });
            for (Showing showing : showings) {
                showList.addItem(showing);
            }
        }
    }

    private void refreshTextFields() {
        if (showing != null && seatTypeCombo.getSelectedItem() != null) {
            Integer screenNum = bookingSystem.getScreenNumber(showing);
            screenTextField.setText(screenNum.toString());
            Integer seatsRemaining = showing.getUnbookedSeats(seatTypeCombo.getSelectedItem().toString()).size();
            seatsRemainingTextField.setText(seatsRemaining.toString());
            String cost = bookingSystem.toDecimalCurrency(bookingSystem.getPrice(showing, seatTypeCombo.getSelectedItem().toString()), "£");
            costTextField.setText(cost);
        } else {
            screenTextField.setText("");
            seatsRemainingTextField.setText("");
            costTextField.setText("");
        }
    }

    private void initialiseLists() {
        this.thisPanel = (MainUIPanel) newBookingUIPanel;
        this.showDate.getSettings().setDateRangeLimits(LocalDate.now(), null);
        refreshFilmList();
        refreshShowingList();
        setStaticComboBoxes();
        showing = (Showing) showList.getSelectedItem();
        film = (Film) filmList.getSelectedItem();
        refreshTextFields();
    }

    private void initialiseListeners() {
        backButton.addActionListener(new MainUIBackButtonActionListener(thisPanel));
        showDate.addDateChangeListener(new DateChangeListener() {
            @Override
            public void dateChanged(DateChangeEvent dateChangeEvent) {
                refreshFilmList();
                refreshShowingList();
                refreshTextFields();
            }
        });

        filmList.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    film = (Film) e.getItem();
                    refreshShowingList();
                    refreshTextFields();
                } else if (e.getStateChange() == ItemEvent.DESELECTED) {
                    film = null;
                    refreshShowingList();
                    refreshTextFields();
                }
            }
        });
        showList.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    showing = (Showing) e.getItem();
                    refreshTextFields();
                } else if (e.getStateChange() == ItemEvent.DESELECTED) {
                    showing = null;
                    refreshTextFields();
                }
            }
        });
        seatTypeCombo.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    refreshTextFields();
                }
            }
        });
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        createUIComponents();
        newBookingUIPanel.setLayout(new GridLayoutManager(12, 13, new Insets(0, 0, 0, 0), -1, -1));
        newBookingUIPanel.setPreferredSize(new Dimension(600, 400));
        addBookingButton = new JButton();
        addBookingButton.setText("Add Booking");
        newBookingUIPanel.add(addBookingButton, new GridConstraints(10, 1, 1, 11, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        newBookingUIPanel.add(filmList, new GridConstraints(5, 5, 1, 7, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 1, false));
        newBookingUIPanel.add(showList, new GridConstraints(6, 5, 1, 7, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 1, false));
        newBookingUIPanel.add(paymentComboBox, new GridConstraints(7, 5, 1, 7, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 1, false));
        backButton = new JButton();
        backButton.setText("Main Menu");
        newBookingUIPanel.add(backButton, new GridConstraints(1, 1, 1, 4, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label1 = new JLabel();
        label1.setText("Film");
        newBookingUIPanel.add(label1, new GridConstraints(5, 1, 1, 4, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label2 = new JLabel();
        label2.setText("Showing Time");
        newBookingUIPanel.add(label2, new GridConstraints(6, 1, 1, 4, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label3 = new JLabel();
        label3.setText("Payment Method");
        newBookingUIPanel.add(label3, new GridConstraints(7, 1, 1, 4, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        newBookingUIPanel.add(showDate, new GridConstraints(3, 5, 2, 7, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 1, false));
        final Spacer spacer1 = new Spacer();
        newBookingUIPanel.add(spacer1, new GridConstraints(2, 5, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        final Spacer spacer2 = new Spacer();
        newBookingUIPanel.add(spacer2, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        final JLabel label4 = new JLabel();
        label4.setText("Date");
        newBookingUIPanel.add(label4, new GridConstraints(3, 2, 2, 3, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer3 = new Spacer();
        newBookingUIPanel.add(spacer3, new GridConstraints(1, 12, 9, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final Spacer spacer4 = new Spacer();
        newBookingUIPanel.add(spacer4, new GridConstraints(1, 0, 10, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final Spacer spacer5 = new Spacer();
        newBookingUIPanel.add(spacer5, new GridConstraints(0, 0, 1, 13, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        final Spacer spacer6 = new Spacer();
        newBookingUIPanel.add(spacer6, new GridConstraints(11, 0, 1, 13, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        final JLabel label5 = new JLabel();
        label5.setText("Screen");
        newBookingUIPanel.add(label5, new GridConstraints(9, 1, 1, 4, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label6 = new JLabel();
        label6.setText("Seat Type:");
        newBookingUIPanel.add(label6, new GridConstraints(8, 1, 1, 4, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        newBookingUIPanel.add(seatTypeCombo, new GridConstraints(8, 5, 1, 7, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 1, false));
        screenTextField = new JTextField();
        screenTextField.setEditable(false);
        newBookingUIPanel.add(screenTextField, new GridConstraints(9, 5, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(10, -1), null, 2, false));
        final JLabel label7 = new JLabel();
        label7.setText("Seats Remaining");
        newBookingUIPanel.add(label7, new GridConstraints(9, 7, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        seatsRemainingTextField = new JTextField();
        seatsRemainingTextField.setEditable(false);
        newBookingUIPanel.add(seatsRemainingTextField, new GridConstraints(9, 8, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(10, -1), null, 1, false));
        final JLabel label8 = new JLabel();
        label8.setText("Cost");
        newBookingUIPanel.add(label8, new GridConstraints(9, 9, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        costTextField = new JTextField();
        costTextField.setEditable(false);
        newBookingUIPanel.add(costTextField, new GridConstraints(9, 10, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(30, -1), null, 3, false));
        newBookingLabel = new JLabel();
        newBookingLabel.setText("New Booking");
        newBookingUIPanel.add(newBookingLabel, new GridConstraints(1, 5, 1, 6, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return newBookingUIPanel;
    }
}
