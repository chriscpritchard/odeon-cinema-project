package uk.co.christopherpritchard.odeoncinemaprojectGUI;

import uk.co.christopherpritchard.odeoncinemaproject.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

class NewCustomerUI extends NewUserUI {

    private CinemaBookingSystem cinema;
    private User modifyUser;
    private List<NewUserUIListener> listeners;


    public NewCustomerUI(CinemaBookingSystem cinema) {
        super(cinema);
        super.addButtonListener(new NewCustomerButtonListener());
        this.cinema = cinema;
        this.listeners = new ArrayList<>();
        super.setTitle("New Customer - Odeon Cinema System");
        super.setVisible(true);
    }

    public NewCustomerUI(CinemaBookingSystem cinema, Customer customer) {
        super(cinema, customer);
        this.modifyUser = customer;
        super.addButtonListener(new ChangeCustomerButtonListener());
        this.cinema = cinema;
        this.listeners = new ArrayList<>();
        super.setTitle("Change Details - Odeon Cinema System");
        super.setVisible(true);
    }

    @Override
    void addEventListener(NewUserUIListener listener) {
        this.listeners.add(listener);
    }


    class ChangeCustomerButtonListener extends CreateListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                if (modifyUser.authenticate(NewCustomerUI.super.getExistingPassword())){
                    if (NewCustomerUI.super.checkPasswordFields()){
                        if (NewCustomerUI.this.getPasswordText().length == 0){
                            cinema.changeUser(modifyUser, NewCustomerUI.this.getUsernameText(), NewCustomerUI.this.getFirstNameText(), NewCustomerUI.this.getLastNameText(), NewCustomerUI.super.getExistingPassword());
                            listeners.forEach(NewUserUIListener::userModified);
                            NewCustomerUI.super.dispose();
                        } else {
                            cinema.changeUser(modifyUser, NewCustomerUI.this.getUsernameText(), NewCustomerUI.this.getFirstNameText(), NewCustomerUI.this.getLastNameText(),NewCustomerUI.super.getPasswordText(), NewCustomerUI.super.getExistingPassword());
                            listeners.forEach(NewUserUIListener::userModified);
                            NewCustomerUI.super.dispose();
                        }
                    } else {
                        throw new UserAddFailedException("The passwords do not match");
                    }
                    NewCustomerUI.super.clearPassword();
                }else{
                    throw new UserAddFailedException("Please enter existing password");
                }
            }catch (InvalidLoginException | UserAddFailedException ex){
                JOptionPane.showMessageDialog(NewCustomerUI.this, ex.getMessage(), "Error modifying user", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    class NewCustomerButtonListener extends CreateListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            String username = NewCustomerUI.super.getUsernameText();
            try{
                if(NewCustomerUI.super.checkPasswordFields()){
                    cinema.addCustomer(username, NewCustomerUI.super.getFirstNameText(), NewCustomerUI.super.getLastNameText(), NewCustomerUI.super.getPasswordText());
                    JOptionPane.showMessageDialog(NewCustomerUI.this, "Successfully created user: " + username, "User created", JOptionPane.INFORMATION_MESSAGE);
                    listeners.forEach(o->o.newUserCreated(username));
                    NewCustomerUI.super.dispose();
                }else {
                    throw new UserAddFailedException("The passwords do not match");
                }
            } catch (UserAddFailedException ex) {
                NewCustomerUI.super.clearPassword();
                JOptionPane.showMessageDialog(NewCustomerUI.this, ex.getMessage(), "Error adding user", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        listeners.forEach(NewUserUIListener::newUserUIclosed);
    }
}
