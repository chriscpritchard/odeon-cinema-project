package uk.co.christopherpritchard.odeoncinemaprojectGUI;

import uk.co.christopherpritchard.odeoncinemaproject.*;

import javax.swing.*;

/**
 * GUI Class for the Odeon Cinema System
 */
public class GUI implements UIListeners {
    //the object that implements the CinemaBookingSystem interface
    private CinemaBookingSystem bookingSystem;
    //the currently logged in user
    private User loggedInUser;
    //the LoginUI
    private LoginUI login;
    private JFrame hiddenFrame;


    /**
     * Default constructor for GUI, creates a new Cinema object with default manager username / password combination
     */
    public GUI(){
        this(new Cinema("manager", "manager".toCharArray()));
    }

    /**
     * Constructor for GUI, uses specified CinemaBookingSystem object
     * @param bookingSystem the CinemaBookingSystem to use
     */
    public GUI(CinemaBookingSystem bookingSystem){
        //Sets system look and feel because it looks way better than the default "metal" UI
        systemLookAndFeel();
        //assign the bookingsystem object
        this.bookingSystem = bookingSystem;
        //make a new loginUI
        login = new LoginUI(this.bookingSystem);
        //start listening to the loginUI
        login.addEventListener(this);
        //make the LoginUI visible
        login.setVisible(true);
    }

    @Override
    public void loggedIn(User u) {
        MainUI main;
        this.loggedInUser = u;
        if (loggedInUser.getClass() == Manager.class){
            main = new MainUI(bookingSystem, new ManagerUI(bookingSystem).getPanel());
        }else if (loggedInUser.getClass() == Customer.class){
            main = new MainUI(bookingSystem, new CustomerUI(bookingSystem).getPanel());
        }else{
            login.setVisible(true);
            login.setError("Login Failed: User type not found");
            login.toFront();
            login.requestFocus();
            return;
        }
        main.addEventListener(this);
    }

    @Override
    public void newUserUIclosed() {
        this.hiddenFrame.setVisible(true);
        this.hiddenFrame.toFront();
        this.hiddenFrame.requestFocus();
    }

    public void MainUILoggedOut() {
        bookingSystem.logout();
        this.login.setVisible(true);
        this.login.toFront();
        this.login.requestFocus();
    }

    public void newCustomerPressed(JFrame frame){
        frame.setVisible(false);
        NewCustomerUI newCustomerUI = new NewCustomerUI(bookingSystem);
        newCustomerUI.addEventListener(this);
        this.hiddenFrame = frame;
    }

    @Override
    public void newUserCreated(String username) {
        this.hiddenFrame.setVisible(true);
        this.hiddenFrame.toFront();
        this.hiddenFrame.requestFocus();
    }

    public void userModified() {
        this.hiddenFrame.setVisible(true);
        this.hiddenFrame.toFront();
        this.hiddenFrame.requestFocus();
    }

    /**
     * Sets the look and feel to the system look and feel (if that's not available, use the cross platform look and feel)
     * throws a runtime exception if neither system, nor cross platform, look and feel is available
     */
    private void systemLookAndFeel(){
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (UnsupportedLookAndFeelException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            try {
                UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
            } catch (UnsupportedLookAndFeelException | ClassNotFoundException | InstantiationException | IllegalAccessException f) {
                throw new RuntimeException(f);
            }
        }
    }

}
