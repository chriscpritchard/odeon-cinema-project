package uk.co.christopherpritchard.odeoncinemaprojectGUI;

import uk.co.christopherpritchard.CollectionTools.CollectionToCollection;
import uk.co.christopherpritchard.odeoncinemaproject.*;


import javax.naming.NoPermissionException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * The OdeonCinemaProject Class - contains the main method for entry into the program
 */
public class OdeonCinemaProject {
    private static List<String> reviews = initialiseReviewStrings();
    private static List<String> initialiseReviewStrings(){
        List<String> reviews = new ArrayList<>();
        reviews.add("Poor show, shoddy acting");
        reviews.add("Awesome! Loved it!");
        reviews.add("Meh. That is all.");
        reviews.add("Seat was a bit uncomfortable, and for that I'm going to give this movie a poor review");
        reviews.add("Child behind me wouldn't shut up!");
        reviews.add("I've run out of things to put here");
        reviews.add("Han shot first!");
        reviews.add("Always was a trekkie myself...");
        reviews.add("Does anyone actually read these? I'm not convinced and anyway, as a customer I'm always right!");
        return reviews;
    }
    public static void main(String[] args){
        boolean doTest = false;
        boolean interactive = true;
        for (String arg : args){
            if (arg.equals("test")){
                doTest = true;
            } if (arg.equals("nokeys")){
                interactive = false;
            }
        }
        if (doTest){
            initialiseTest(interactive);
        } else {
            initialiseGUI();
        }
    }
    private static void initialiseTest(boolean interactive){
        System.out.println("***ODEON CINEMA SYSTEM TESTING***");
        System.out.println("Creating CinemaTest object with a new Cinema");
        System.out.println("Manager username: testmanager");
        System.out.println("Manager password: testmanager");
        CinemaTest tester = new CinemaTest(new Cinema("testmanager","testmanager".toCharArray()), interactive);
        System.out.println("CinemaTest object created");
        if (interactive){
        System.out.println("Press enter key to begin the test...");
        try
        {
            System.in.read();
            int count = System.in.available();
            System.in.skip(count);
        }
        catch(Exception e)
        {

        }
        }
        tester.doTest();
    }
    private static void initialiseGUI(){
        System.out.println("Generating initial data...");
        CinemaBookingSystem bookingSystem = new Cinema("manager", "manager".toCharArray());
        try{
            bookingSystem.login("manager", "manager".toCharArray());
            addFilms(bookingSystem);
            addUsers(bookingSystem);
            addShowings(bookingSystem);
            addBookings(bookingSystem);
            addReviews(bookingSystem);
            System.out.println("...Initial data loaded into system");
        } catch (InvalidLoginException |NoPermissionException | UserAddFailedException e){
            System.err.println("Error:" + e.getMessage());
            return;
        }
        bookingSystem.logout();
        GUI gui = new GUI(bookingSystem);
    }

    private static void addFilms(CinemaBookingSystem bookingSystem) throws NoPermissionException{
        //Exact lengths + 10 mins for ads - (un)fortunately this cinema only shows Star Wars!
        bookingSystem.addFilm("Star Wars Episode I: The Phantom Menace", Duration.ofMinutes((long) 146));
        bookingSystem.addFilm("Star Wars Episode II: Attack of the Clones", Duration.ofMinutes((long) 152));
        bookingSystem.addFilm("Star Wars Episode III: Revenge of the Sith", Duration.ofMinutes((long) 150));
        bookingSystem.addFilm("Rogue One: A Star Wars Story", Duration.ofMinutes((long) 143));
        bookingSystem.addFilm("Star Wars Episode IV: A New Hope", Duration.ofMinutes((long) 135));
        bookingSystem.addFilm("Star Wars Episode V: Empire Strikes Back", Duration.ofMinutes((long) 137));
        bookingSystem.addFilm("Star Wars Episode VI: Return of the Jedi", Duration.ofMinutes((long) 146));
        bookingSystem.addFilm("Star Wars Episode VII: The Force Awakens", Duration.ofMinutes((long) 145));
        bookingSystem.addFilm("Star Wars Episode VIII: The Last Jedi", Duration.ofMinutes((long) 163));
    }

    private static void addUsers(CinemaBookingSystem bookingSystem) throws UserAddFailedException{
        bookingSystem.addCustomer("customer", "Default", "Customer", "customer".toCharArray());
        bookingSystem.addCustomer("tsmith", "Tom", "Smith", "password1".toCharArray());
        bookingSystem.addCustomer("cpritchard", "Chris", "Pritchard", "password2".toCharArray());
        bookingSystem.addCustomer("ajones", "Amanda", "Jones", "password3".toCharArray());
        bookingSystem.addCustomer("tpaddock", "Thomas", "Paddock", "password4".toCharArray());
        bookingSystem.addCustomer("pjones", "Phil", "Jones", "password5".toCharArray());
        bookingSystem.addCustomer("kgreen", "Karen", "Green", "!\"£$%^&*()".toCharArray());
        bookingSystem.addCustomer("AUser", "Theodophillus", "Wildebeste", "password7".toCharArray());
        bookingSystem.addCustomer("ashah", "Alpa", "Shah", "password8".toCharArray());
        bookingSystem.addCustomer("mdepiffel", "Maximillian", "De Piffel", "password9".toCharArray());
        bookingSystem.addCustomer("aperson", "Suzanne", "O'Sullivan", "password10".toCharArray());
        bookingSystem.addCustomer("ymckenna", "Yvonne", "McKenna", "password11".toCharArray());
        bookingSystem.addCustomer("ksteele", "Kate", "Steele", "password12".toCharArray());
        bookingSystem.addCustomer("jrobinson", "Jackie", "Robinson", "password13".toCharArray());
        bookingSystem.addCustomer("hennerz", "Henry", "Livingstone", "password6".toCharArray());
        bookingSystem.addCustomer("CGIRulez", "George", "Lucas", "password14".toCharArray());
        bookingSystem.addCustomer("LensFlareLover", "Jeffrey", "Abrams", "password14".toCharArray());
        bookingSystem.addCustomer("incredibles", "Brad", "Bird", "password14".toCharArray());
        bookingSystem.addCustomer("captainjacksparrow", "Michael", "Bolton", "password14".toCharArray());
        bookingSystem.addCustomer("KesselRunWinner", "Harrison", "Ford", "chewie".toCharArray());
        bookingSystem.addCustomer("obi1", "Benjamin", "Kenobi", "usetheforceluke".toCharArray());
        bookingSystem.addCustomer("greenman52", "Master", "Yoda", "doordonotthereisnotry".toCharArray());
        bookingSystem.addCustomer("lotrman72", "Peter", "Jackson", "braindeadwasmygreatestcreation".toCharArray());
        bookingSystem.addCustomer("Darth_Vader", "Anakin", "Skywalker", "IAmLukesFather".toCharArray());
        bookingSystem.addCustomer("jedi_master", "Luke", "Skywalker", "WhyDidNoOneTellMeSheWasMySister".toCharArray());
        bookingSystem.addCustomer("DualSaberMan", "Darth", "Maul", "IKilledQuiGon".toCharArray());
        bookingSystem.addCustomer("emokid52", "Kylo", "Ren", "mcrrulez<3".toCharArray());
        bookingSystem.addCustomer("beepboopbeep", "R2", "D2", "boopbeepboop".toCharArray());
        bookingSystem.addCustomer("Protocol_Droid", "C", "3PO", "ohmygoodness".toCharArray());
        bookingSystem.addCustomer("purplesaber72", "Mace", "Windu", "mylightsaberispurple".toCharArray());
        bookingSystem.addCustomer("dooku", "Christopher", "Lee", "saruman01".toCharArray());
        bookingSystem.addCustomer("gunganrulez", "Jar Jar", "Binks", "mesajarjar".toCharArray());
        bookingSystem.addCustomer("supremeleader", "Andy", "Serkis", "myprecious".toCharArray());
        bookingSystem.addCustomer("TheAllSeeingEye", "Dark Lord", "Sauron", "oneringtorulethemall".toCharArray());
        bookingSystem.addCustomer("just_a_gardener", "Samwise", "Gamgee", "masterfrodo".toCharArray());
        bookingSystem.addCustomer("BestCaptain", "Katherine", "Janeway", "voyager52".toCharArray());
        bookingSystem.addCustomer("CaptainMercer", "Seth", "Macfarlane", "whykellywhy".toCharArray());
        bookingSystem.addCustomer("leslie_knope", "Leslie", "Knope", "ILovePawnee".toCharArray());
        bookingSystem.addCustomer("riker", "William", "Riker", "IStepOverChairs".toCharArray());
        bookingSystem.addCustomer("Scotty", "Montgomery", "Scott", "shecannaetakemuchmorecaptain".toCharArray());
        bookingSystem.addCustomer("one", "Elizabeth", "Windsor", "notyetcharles".toCharArray());
    }

    private static void addShowings(CinemaBookingSystem cinemaBookingSystem){
        CollectionToCollection<Film> filmC2C = new CollectionToCollection<>(cinemaBookingSystem.getFilms());
        List<Film> films =  filmC2C.getList();
        List<Screen> screens = cinemaBookingSystem.getScreens();
        LocalDate startDate = LocalDate.of(2017, 11, 1);
        LocalDate endDate = LocalDate.of(2018,12,1);
        //Below would be what I'd do in java 9
        //startDate.datesUntil(endDate).forEach(o->{
        //for compatibility
        long numDays = ChronoUnit.DAYS.between(startDate, endDate);
        IntStream.iterate(0,i->i+1).limit(numDays).mapToObj(startDate::plusDays).collect(Collectors.toList()).forEach(o-> screens.parallelStream().forEach(p->{
            try {
                cinemaBookingSystem.addShowingAnyway(films.get(ThreadLocalRandom.current().nextInt(0, 9)), p, o.atTime(LocalTime.of(14, 00, 0)), 600);
                cinemaBookingSystem.addShowingAnyway(films.get(ThreadLocalRandom.current().nextInt(0, 9)), p, o.atTime(LocalTime.of(17, 00, 0)), 750);
                cinemaBookingSystem.addShowingAnyway(films.get(ThreadLocalRandom.current().nextInt(0, 9)), p, o.atTime(LocalTime.of(19, 45, 0)), 850);
                cinemaBookingSystem.addShowingAnyway(films.get(ThreadLocalRandom.current().nextInt(0, 9)), p, o.atTime(LocalTime.of(22, 30, 0)), 700);
            } catch (NoPermissionException e) {
                System.err.println("Error:" + e.getMessage());
                return;
            }
        }));
    }

    private static void addBookings(CinemaBookingSystem cinemaBookingSystem){
        //void addBooking(Showing showing, String seatType, Customer customer, Payment payment) throws BookingFailedException, NoPermissionException;
        List<Customer> customers = new CollectionToCollection<>(cinemaBookingSystem.getCustomers()).getList();
        LocalDate startDate = LocalDate.of(2017, 11, 1);
        LocalDate endDate = LocalDate.of(2018,3,1);
        //java 9 code below
        //startDate.datesUntil(endDate).forEach(o-> {
        //do this instead for compatibility
        long numDays = ChronoUnit.DAYS.between(startDate, endDate);
        IntStream.iterate(0,i->i+1).limit(numDays).mapToObj(startDate::plusDays).collect(Collectors.toList()).forEach(o-> customers.forEach(p -> {
            int rand = ThreadLocalRandom.current().nextInt(0, 99);
            List<Showing> showingList = new CollectionToCollection<>(cinemaBookingSystem.getShowings(o)).getList();
            Showing showing = showingList.get(ThreadLocalRandom.current().nextInt(0,showingList.size()));
            if ( rand < 20) {
                try {
                    cinemaBookingSystem.addBookingAnyway(showing, "Standard", p, cinemaBookingSystem.pay("Standard", showing, "Card"));
                } catch (NoPermissionException | BookingFailedException e){
                    System.err.println("Error:" + e.getMessage());
                }
            } else if (rand < 40){
                try {
                    cinemaBookingSystem.addBookingAnyway(showing, "Standard", p, cinemaBookingSystem.pay("Standard", showing, "Cash"));
                } catch (NoPermissionException | BookingFailedException e){
                    System.err.println("Error:" + e.getMessage());
                }
            } else if (rand < 45){
                try {
                    cinemaBookingSystem.addBookingAnyway(showing, "VIP", p, cinemaBookingSystem.pay("VIP", showing, "Card"));
                } catch (NoPermissionException | BookingFailedException e){
                    System.err.println("Error:" + e.getMessage());
                }
            } else if (rand <50){
                try {
                    cinemaBookingSystem.addBookingAnyway(showing, "VIP", p, cinemaBookingSystem.pay("VIP", showing, "Cash"));
                } catch (NoPermissionException | BookingFailedException e){
                    System.err.println("Error:" + e.getMessage());
                }
            }
        }));

    }

    private static void addReviews(CinemaBookingSystem cinemaBookingSystem) throws UserAddFailedException{
        cinemaBookingSystem.getCustomers().forEach(o-> o.getBookings().forEach(p->{
            try {
                cinemaBookingSystem.addReview(p, (byte) ThreadLocalRandom.current().nextInt(1, 6), randomReviewString(), o);
            }catch (NoPermissionException e){
                System.err.println("Error:" + e.getMessage());
            }
        }));
        cinemaBookingSystem.addCustomer("king", "Charles", "Windsor", "oneday".toCharArray());
    }

    private static String randomReviewString(){
        return reviews.get(ThreadLocalRandom.current().nextInt(0,reviews.size()));
    }
}
