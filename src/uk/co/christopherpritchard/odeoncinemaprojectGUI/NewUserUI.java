package uk.co.christopherpritchard.odeoncinemaprojectGUI;

import uk.co.christopherpritchard.odeoncinemaproject.CinemaBookingSystem;
import uk.co.christopherpritchard.odeoncinemaproject.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

abstract class NewUserUI extends JFrame {

    /**
     * Assistance with layout recieved from: https://stackoverflow.com/questions/21217806/java-swing-boxlayout
     *
     * */
    private static final int COLUMNS = 15;
    private static final int GAP = 4;
    private static final Insets LABEL_INSETS = new Insets(GAP, GAP, GAP, 15);
    private static final Insets FIELD_INSETS = new Insets(GAP, GAP, GAP, GAP);
    private static final Insets BUTTON_INSETS= new Insets(GAP, 25, GAP, 25);

    private CinemaBookingSystem cinema;
    private JPanel panel;
    private JLabel usernameLabel;
    private JTextField usernameField;
    private JLabel passwordLabel;
    private JPasswordField passwordField;
    private JLabel passwordLabel2;
    private JPasswordField passwordField2;
    private JLabel firstNameLabel;
    private JTextField firstNameField;
    private JLabel lastNameLabel;
    private JTextField lastNameField;
    private JButton createButton;
    private JButton closeButton;
    private JPasswordField existingPasswordField;
    private JLabel existingPasswordLabel;

    NewUserUI(CinemaBookingSystem cinema){
        this.cinema = cinema;
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setResizable(false);

        this.panel = new JPanel();
        this.panel.setLayout(new GridBagLayout());


        this.usernameLabel = new JLabel("Username: ");
        this.usernameField = new JTextField(COLUMNS);
        this.passwordLabel = new JLabel("Password: ");
        this.passwordField = new JPasswordField(COLUMNS);
        this.passwordLabel2 = new JLabel("Repeat Password: ");
        this.passwordField2 = new JPasswordField(COLUMNS);
        this.firstNameLabel = new JLabel("First Name: ");
        this.firstNameField = new JTextField(COLUMNS);
        this.lastNameLabel = new JLabel("Last Name: ");
        this.lastNameField = new JTextField(COLUMNS);


        addLabel(firstNameLabel, 1);
        addField(firstNameField, 1);
        addLabel(lastNameLabel, 2);
        addField(lastNameField, 2);
        addLabel(usernameLabel, 3);
        addField(usernameField, 3);
        addLabel(passwordLabel, 4);
        addField(passwordField, 4);
        addLabel(passwordLabel2, 5);
        addField(passwordField2, 5);

        this.closeButton = new JButton("Back");
        addButton(closeButton, 0, 6);
        closeButton.addActionListener(new CloseListener());
        this.createButton = new JButton("Create");
        addButton(createButton, 1, 6);

        this.getContentPane().add(panel, BorderLayout.CENTER);
        this.setLocationRelativeTo(null);
        this.setSize(300,300);
    }

    NewUserUI(CinemaBookingSystem cinema, User user) {
        this.cinema = cinema;
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setResizable(false);

        this.panel = new JPanel();
        this.panel.setLayout(new GridBagLayout());


        this.usernameLabel = new JLabel("Username: ");
        this.usernameField = new JTextField(COLUMNS);
        this.passwordLabel = new JLabel("Password: ");
        this.passwordField = new JPasswordField(COLUMNS);
        this.passwordLabel2 = new JLabel("Repeat Password: ");
        this.passwordField2 = new JPasswordField(COLUMNS);
        this.existingPasswordLabel = new JLabel("Existing Password: ");
        this.existingPasswordField = new JPasswordField(COLUMNS);
        this.firstNameLabel = new JLabel("First Name: ");
        this.firstNameField = new JTextField(COLUMNS);
        this.lastNameLabel = new JLabel("Last Name: ");
        this.lastNameField = new JTextField(COLUMNS);

        this.usernameField.setText(user.getUsername());
        this.usernameField.setEditable(false);
        this.firstNameField.setText(user.getFirstname());
        this.lastNameField.setText(user.getLastname());

        addLabel(firstNameLabel, 1);
        addField(firstNameField, 1);
        addLabel(lastNameLabel, 2);
        addField(lastNameField, 2);
        addLabel(usernameLabel, 3);
        addField(usernameField, 3);
        addLabel(existingPasswordLabel, 4);
        addField(existingPasswordField, 4);
        addLabel(passwordLabel, 5);
        addField(passwordField, 5);
        addLabel(passwordLabel2, 6);
        addField(passwordField2, 6);
        this.closeButton = new JButton("Back");
        addButton(closeButton, 0, 7);
        this.createButton = new JButton("Modify");
        addButton(createButton, 1, 7);

        closeButton.addActionListener(new CloseListener());
        this.getContentPane().add(panel, BorderLayout.CENTER);
        this.setLocationRelativeTo(null);
        this.setSize(300,300);
    }




    abstract void addEventListener(NewUserUIListener listener);


    String getUsernameText() {
        return usernameField.getText();
    }

    char[] getPasswordText() {
        return passwordField.getPassword();
    }

    char[] getExistingPassword(){
        return existingPasswordField.getPassword();
    }

    String getFirstNameText() {
        return firstNameField.getText();
    }

    String getLastNameText() {
        return lastNameField.getText();
    }

    void clearPassword(){
        if (existingPasswordField != null){
            existingPasswordField.setText("");
        }
        passwordField.setText("");
        passwordField2.setText("");
    }

    boolean checkPasswordFields(){
        return Arrays.equals(passwordField.getPassword(), passwordField2.getPassword());
    }


    void addButtonListener(CreateListener buttonListener){
        createButton.addActionListener(buttonListener);
    }

    private void addLabel(JLabel jLabel, int row){
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.gridx = 0;
        gbc.gridy = row;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.insets = LABEL_INSETS;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        panel.add(jLabel, gbc);
    }

    private void addField(JTextField jTextField, int row){
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.gridx = 1;
        gbc.gridy = row;
        gbc.anchor = GridBagConstraints.EAST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = FIELD_INSETS;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        panel.add(jTextField, gbc);
    }

    private void addButton(JButton button, int col, int row){
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.gridx = col;
        gbc.gridy = row;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = BUTTON_INSETS;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        panel.add(button, gbc);
    }

    abstract class CreateListener implements ActionListener{

    }

    class CloseListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            NewUserUI.this.dispose();
        }
    }
}
