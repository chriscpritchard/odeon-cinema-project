package uk.co.christopherpritchard.odeoncinemaprojectGUI;

public interface NewUserUIListener {
    void newUserCreated(String un);
    void newUserUIclosed();
    void userModified();
}
