package uk.co.christopherpritchard.odeoncinemaprojectGUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainUIBackButtonActionListener implements ActionListener {
    private MainUIPanel thisPanel;

    public MainUIBackButtonActionListener(MainUIPanel thisPanel) {
        this.thisPanel = thisPanel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        thisPanel.uiBack(thisPanel);
    }
}

