package uk.co.christopherpritchard.odeoncinemaprojectGUI;

import uk.co.christopherpritchard.odeoncinemaproject.*;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class MainUI extends JFrame implements MainUIPanelListener, NewUserUIListener {
    private CinemaBookingSystem bookingSystem;
    private List<MainUIListener> listeners;
    private MainUIPanel newBookingUIPanel;
    private MainUIPanel addModifyReviewUIPanel;
    private MainUIPanel panel;
    private MainUIPanel custFutureBookingsUIPanel;
    private MainUIPanel custPastBookingsUIPanel;
    private MainUIPanel filmReviewUIPanel;
    private MainUIPanel addFilmUIPanel;
    private MainUIPanel addShowingPanel;

    MainUI(CinemaBookingSystem bookingSystem, MainUIPanel panel){
        this.panel = panel;
        this.bookingSystem = bookingSystem;
        this.listeners = new ArrayList<>();
        this.setTitle("Odeon Cinema System - " + bookingSystem.getAuthenticatedUser().getFirstname() + " " + bookingSystem.getAuthenticatedUser().getLastname());
        this.setContentPane(this.getContentPane());
        this.add(panel);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        panel.addEventListener(this);
        this.setLocationRelativeTo(null);
        //this.setState(Frame.MAXIMIZED_BOTH);
        this.pack();
        //help recieved from: https://stackoverflow.com/questions/6790600/java-gui-hides-windows-taskbar - avoids maximising covering taskbar
        GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
        this.setMaximizedBounds(env.getMaximumWindowBounds());
        this.setExtendedState(this.getExtendedState() | MAXIMIZED_BOTH);
        setResizable(true);
        this.setVisible(true);

    }

    void addEventListener(MainUIListener listener){
        listeners.add(listener);
    }

    @Override
    public void newBooking() {
        newBooking((Customer) this.bookingSystem.getAuthenticatedUser());
    }

    @Override
    public void newBooking(Customer customer) {
        if (newBookingUIPanel != null) {
            newBookingUIPanel.removeEventListener(this);
        }
        this.newBookingUIPanel = new NewBookingUI(bookingSystem, customer).getPanel();
        newBookingUIPanel.addEventListener(this);
        this.remove(panel);
        this.add(newBookingUIPanel);
        this.repaint();
        //this.pack();
        this.validate();
    }


    @Override
    public void modifyBooking(Booking booking, JPanel panel, Customer customer) {
        if (newBookingUIPanel != null) {
            newBookingUIPanel.removeEventListener(this);
        }
        this.newBookingUIPanel = new NewBookingUI(bookingSystem, booking, customer).getPanel();
        newBookingUIPanel.addEventListener(this);
        this.remove(panel);
        this.add(newBookingUIPanel);
        this.repaint();
        this.validate();
        //this.pack();
    }

    @Override
    public void mainUIPanelLoggedOut() {
        this.dispose();
    }

    @Override
    public void customerUIAddModifyReview(Film film) {
        this.remove(panel);
        if (addModifyReviewUIPanel != null) {
            addModifyReviewUIPanel.removeEventListener(this);
        }
        this.addModifyReviewUIPanel = new AddModifyReviewUI(bookingSystem, film).getPanel();
        addModifyReviewUIPanel.addEventListener(this);
        this.add(addModifyReviewUIPanel);
        this.repaint();
        this.validate();
        //this.pack();
    }

    @Override
    public void uiBack(JPanel oldPanel) {
        this.remove(oldPanel);
        panel.updateUI();
        this.add(panel);
        this.repaint();
        this.validate();
        //this.pack();
    }

    @Override
    public void futureBookings() {
        futureBookings((Customer) this.bookingSystem.getAuthenticatedUser());
    }

    @Override
    public void futureBookings(Customer cust) {
        this.remove(panel);
        if (custFutureBookingsUIPanel != null){
            custFutureBookingsUIPanel.removeEventListener(this);
        }
        this.custFutureBookingsUIPanel = new futureBookingsUI(bookingSystem, cust).getPanel();
        this.custFutureBookingsUIPanel.addEventListener(this);
        this.add(custFutureBookingsUIPanel);
        this.repaint();
        this.validate();
        //this.pack();
    }

    @Override
    public void pastBookings() {
        pastBookings((Customer) this.bookingSystem.getAuthenticatedUser());
    }

    @Override
    public void pastBookings(Customer cust) {
        this.remove(panel);
        if (custPastBookingsUIPanel != null){
            custPastBookingsUIPanel.removeEventListener(this);
        }
        this.custPastBookingsUIPanel = new pastBookingsUI(bookingSystem, cust).getPanel();
        this.custPastBookingsUIPanel.addEventListener(this);
        this.add(custPastBookingsUIPanel);
        this.repaint();
        this.validate();
        //this.pack();
    }

    public void modifyCustomer(){
        NewUserUI modUI;
        this.setVisible(false);
        modUI = new NewCustomerUI(this.bookingSystem, (Customer) bookingSystem.getAuthenticatedUser());
        modUI.setVisible(true);
        modUI.addEventListener(this);
    }

    public void modifyManager(){
        NewUserUI modUI;
        this.setVisible(false);
        modUI = new NewManagerUI(this.bookingSystem, (Manager) bookingSystem.getAuthenticatedUser());
        modUI.setVisible(true);
        modUI.addEventListener(this);
    }

    public void addManager(){
        NewUserUI addUI;
        this.setVisible(false);
        addUI = new NewManagerUI(this.bookingSystem);
        addUI.setVisible(true);
        addUI.addEventListener(this);
    }

    @Override
    public void filmReview(Film film) {
        this.remove(panel);
        if (filmReviewUIPanel != null){
            filmReviewUIPanel.removeEventListener(this);
        }
        this.filmReviewUIPanel = new FilmReviewUI(bookingSystem, film).getPanel();
        this.filmReviewUIPanel.addEventListener(this);
        this.add(filmReviewUIPanel);
        this.repaint();
        this.validate();
    }

    @Override
    public void addFilm() {
        this.remove(panel);
        if (addFilmUIPanel != null){
            addFilmUIPanel.removeEventListener(this);
        }
        this.addFilmUIPanel = new AddFilmUI(bookingSystem).getPanel();
        this.addFilmUIPanel.addEventListener(this);
        this.add(addFilmUIPanel);
        this.repaint();
        this.validate();
    }

    @Override
    public void addShowing() {
        this.remove(panel);
        if (addShowingPanel != null){
            addShowingPanel.removeEventListener(this);
        }
        this.addShowingPanel = new AddShowingUI(bookingSystem).getPanel();
        this.addShowingPanel.addEventListener(this);
        this.add(addShowingPanel);
        this.repaint();
        this.validate();
    }

    @Override
    public void addShowing(Film film) {
        this.remove(panel);
        if (addShowingPanel != null){
            addShowingPanel.removeEventListener(this);
        }
        this.addShowingPanel = new AddShowingUI(bookingSystem, film).getPanel();
        this.addShowingPanel.addEventListener(this);
        this.add(addShowingPanel);
        this.repaint();
        this.validate();
    }

    @Override
    public void addShowing(Screen screen) {
        this.remove(panel);
        if (addShowingPanel != null){
            addShowingPanel.removeEventListener(this);
        }
        this.addShowingPanel = new AddShowingUI(bookingSystem, screen).getPanel();
        this.addShowingPanel.addEventListener(this);
        this.add(addShowingPanel);
        this.repaint();
        this.validate();
    }

    @Override
    public void newUserCreated(String un) {
        this.setVisible(true);
        this.toFront();
        this.requestFocus();
    }

    @Override
    public void newUserUIclosed() {
        this.setTitle("Odeon Cinema System - " + bookingSystem.getAuthenticatedUser().getFirstname() + " " + bookingSystem.getAuthenticatedUser().getLastname());
        this.setVisible(true);
        this.toFront();
        this.requestFocus();
    }

    @Override
    public void userModified() {
        this.setTitle("Odeon Cinema System - " + bookingSystem.getAuthenticatedUser().getFirstname() + " " + bookingSystem.getAuthenticatedUser().getLastname());
        this.setVisible(true);
        this.toFront();
        this.requestFocus();
    }

    @Override
    public void dispose() {
        super.dispose();
        listeners.forEach(MainUIListener::MainUILoggedOut);
    }
}
