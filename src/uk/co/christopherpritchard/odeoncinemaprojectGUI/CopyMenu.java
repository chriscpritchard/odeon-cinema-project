package uk.co.christopherpritchard.odeoncinemaprojectGUI;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.event.ActionEvent;

public class CopyMenu extends JPopupMenu {

    CopyMenu(JTextComponent text){
        super();
        Action copyAction = new AbstractAction("Copy") {
            @Override
            public void actionPerformed(ActionEvent e) {
                text.copy();
            }
        };
        JMenuItem copy = new JMenuItem(copyAction);
        this.add(copy);
    }
}
