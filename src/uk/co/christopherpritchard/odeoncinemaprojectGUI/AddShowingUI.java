package uk.co.christopherpritchard.odeoncinemaprojectGUI;

import com.github.lgooddatepicker.components.DateTimePicker;
import com.github.lgooddatepicker.optionalusertools.DateTimeChangeListener;
import com.github.lgooddatepicker.zinternaltools.DateTimeChangeEvent;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import uk.co.christopherpritchard.CollectionTools.CollectionToCollection;
import uk.co.christopherpritchard.odeoncinemaproject.CinemaBookingSystem;
import uk.co.christopherpritchard.odeoncinemaproject.Film;
import uk.co.christopherpritchard.odeoncinemaproject.Screen;
import uk.co.christopherpritchard.odeoncinemaproject.Showing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import javax.naming.NoPermissionException;
import javax.swing.*;
import java.awt.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAdjuster;
import java.util.Vector;

public class AddShowingUI {

    private JPanel addShowingPanel;
    private JButton mainMenuButton;
    private JComboBox<Film> filmComboBox;
    private JComboBox<Screen> screenComboBox;
    private JList<Showing> showList;
    private JButton submitButton;
    private DateTimePicker dateTimePicker;
    private JFormattedTextField costField;
    private MainUIPanel thisPanel;
    private CinemaBookingSystem bookingSystem;

    public AddShowingUI(CinemaBookingSystem bookingSystem) {
        $$$setupUI$$$();
        this.bookingSystem = bookingSystem;
        thisPanel = (MainUIPanel) addShowingPanel;
        dateTimePicker.getDatePicker().setDateToToday();
        dateTimePicker.getDatePicker().getSettings().setDateRangeLimits(LocalDate.now(), null);
        dateTimePicker.getDatePicker().getSettings().setAllowEmptyDates(false);
        dateTimePicker.getTimePicker().getSettings().setFormatForDisplayTime("HH:mm");
        dateTimePicker.getTimePicker().getSettings().setFormatForMenuTimes("HH:mm");
        //Help recieved from: https://stackoverflow.com/questions/37613071/java-8-localdatetime-round-to-next-x-minutes
        dateTimePicker.getTimePicker().setTime(LocalTime.now().with(new TemporalAdjuster() {
            @Override
            public Temporal adjustInto(Temporal temporal) {
                int min = temporal.get(ChronoField.MINUTE_OF_HOUR);
                int nextMin = (int) Math.ceil(min / 30d) * 30;
                int adj = nextMin - min;
                return temporal.plus(adj, ChronoUnit.MINUTES).with(ChronoField.SECOND_OF_MINUTE, 0).with(ChronoField.NANO_OF_SECOND, 0);
            }
        }));
        dateTimePicker.getTimePicker().getSettings().setAllowEmptyTimes(false);
        populateScreens();
        populateFilms();
        populateShowings();
        mainMenuButton.addActionListener(new MainUIBackButtonActionListener(thisPanel));
        submitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LocalDateTime dateTime = dateTimePicker.getDateTimePermissive();
                Film film = filmComboBox.getItemAt(filmComboBox.getSelectedIndex());
                Screen screen = screenComboBox.getItemAt(screenComboBox.getSelectedIndex());
                try {
                    bookingSystem.addShowing(film, screen, dateTime);
                    JOptionPane.showMessageDialog(AddShowingUI.this.getPanel(), "Successfully added showing", "Showing added", JOptionPane.INFORMATION_MESSAGE);
                    thisPanel.uiBack(thisPanel);
                } catch (IllegalArgumentException | NoPermissionException ex) {
                    JOptionPane.showMessageDialog(AddShowingUI.this.getPanel(), ex.getMessage(), "Error adding showing", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        dateTimePicker.addDateTimeChangeListener(new DateTimeChangeListener() {
            @Override
            public void dateOrTimeChanged(DateTimeChangeEvent dateTimeChangeEvent) {
                populateShowings();
            }
        });
    }

    public AddShowingUI(CinemaBookingSystem bookingSystem, Film film) {
        this(bookingSystem);
        this.filmComboBox.setSelectedItem(film);
    }

    public AddShowingUI(CinemaBookingSystem bookingSystem, Screen screen) {
        this(bookingSystem);
        this.screenComboBox.setSelectedItem(screen);
    }

    public MainUIPanel getPanel() {
        return thisPanel;
    }

    private void populateScreens() {
        Object sel = screenComboBox.getSelectedItem();
        screenComboBox.removeAllItems();
        List<Screen> screens = bookingSystem.getScreens();
        for (Screen screen : screens) {
            screenComboBox.addItem(screen);
        }
        if (sel != null) {
            screenComboBox.setSelectedItem(sel);
        }
    }

    private void populateFilms() {
        Object sel = filmComboBox.getSelectedItem();
        filmComboBox.removeAllItems();
        List<Film> films = new CollectionToCollection<>(bookingSystem.getFilms()).getSortedList(new Comparator<Film>() {
            @Override
            public int compare(Film o1, Film o2) {
                return o1.getName().compareToIgnoreCase(o2.getName());
            }
        });
        for (Film film : films) {
            filmComboBox.addItem(film);
        }
        if (sel != null) {
            filmComboBox.setSelectedItem(sel);
        }
    }

    private void populateShowings() {
        showList.removeAll();
        Vector<Showing> showVect = new Vector<>();
        showVect.addAll(new CollectionToCollection<>(bookingSystem.getShowings(dateTimePicker.getDatePicker().getDate())).getSortedList(new Comparator<Showing>() {
            @Override
            public int compare(Showing o1, Showing o2) {
                int comp;
                comp = o1.getDatetime().compareTo(o2.getDatetime());
                if (comp == 0) {
                    comp = o1.getScreen().getId() - o2.getScreen().getId();
                }
                return comp;
            }
        }));
        showList.setListData(showVect);
    }

    private void createUIComponents() {
        addShowingPanel = new MainUIPanel();
        showList = new JList<>();
        showList.setCellRenderer(new ShowingCellRendererNew());
        filmComboBox = new JComboBox<>();
        screenComboBox = new JComboBox<>();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        createUIComponents();
        addShowingPanel.setLayout(new GridLayoutManager(10, 6, new Insets(0, 0, 0, 0), -1, -1));
        mainMenuButton = new JButton();
        mainMenuButton.setText("Main Menu");
        addShowingPanel.add(mainMenuButton, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer1 = new Spacer();
        addShowingPanel.add(spacer1, new GridConstraints(1, 2, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final Spacer spacer2 = new Spacer();
        addShowingPanel.add(spacer2, new GridConstraints(0, 0, 9, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final Spacer spacer3 = new Spacer();
        addShowingPanel.add(spacer3, new GridConstraints(0, 5, 9, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final JLabel label1 = new JLabel();
        label1.setText("Existing Showings:");
        addShowingPanel.add(label1, new GridConstraints(1, 4, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JScrollPane scrollPane1 = new JScrollPane();
        addShowingPanel.add(scrollPane1, new GridConstraints(2, 4, 7, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        scrollPane1.setViewportView(showList);
        final JLabel label2 = new JLabel();
        label2.setText("Date and Time");
        addShowingPanel.add(label2, new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label3 = new JLabel();
        label3.setText("Film");
        addShowingPanel.add(label3, new GridConstraints(4, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label4 = new JLabel();
        label4.setText("Screen");
        addShowingPanel.add(label4, new GridConstraints(5, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        addShowingPanel.add(screenComboBox, new GridConstraints(5, 2, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        addShowingPanel.add(filmComboBox, new GridConstraints(4, 2, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        dateTimePicker = new DateTimePicker();
        addShowingPanel.add(dateTimePicker, new GridConstraints(3, 2, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final Spacer spacer4 = new Spacer();
        addShowingPanel.add(spacer4, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, new Dimension(-1, 15), null, null, 0, false));
        final Spacer spacer5 = new Spacer();
        addShowingPanel.add(spacer5, new GridConstraints(9, 0, 1, 6, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, new Dimension(-1, 15), null, null, 0, false));
        final Spacer spacer6 = new Spacer();
        addShowingPanel.add(spacer6, new GridConstraints(8, 2, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        final Spacer spacer7 = new Spacer();
        addShowingPanel.add(spacer7, new GridConstraints(2, 1, 1, 3, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        submitButton = new JButton();
        submitButton.setText("Submit");
        addShowingPanel.add(submitButton, new GridConstraints(7, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return addShowingPanel;
    }

    class ShowingCellRendererNew extends DefaultListCellRenderer {
        @Override
        public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            if (value instanceof Showing) {
                Showing show = (Showing) value;
                String sb = show.getScreen().toString() + " - " +
                        show.getFilm() + " - " +
                        show.getDatetime().format(DateTimeFormatter.ofPattern("HH:mm")) + " - " + show.getDatetime().plusMinutes(show.getFilm().getDuration().toMinutes()).format(DateTimeFormatter.ofPattern("HH:mm")) + " (" + show.getFilm().getDuration().toMinutes() + " minutes)";
                setText(sb);
            }
            return this;
        }
    }


}
