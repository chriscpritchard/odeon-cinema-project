package uk.co.christopherpritchard.odeoncinemaprojectGUI;

import uk.co.christopherpritchard.odeoncinemaproject.CinemaBookingSystem;
import uk.co.christopherpritchard.odeoncinemaproject.InvalidLoginException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

/**
 * A Login User Interface
 */
class LoginUI extends JFrame{

    private CinemaBookingSystem cinema;
    private JPanel panel;
    private JPanel loginFailedPanel;
    private JPanel usernamePanel;
    private JPanel passwordPanel;
    private JPanel buttonPanel;
    private JLabel loginFailedLabel;
    private JLabel usernameLabel;
    private JTextField usernameField;
    private JLabel passwordLabel;
    private JPasswordField passwordField;
    private JButton loginButton;
    private JButton newCustomerButton;
    //a list of objects listening for events
    private java.util.List<LoginUIListener> listeners;

    /**
     * Code to display the login UI
     */
    LoginUI(CinemaBookingSystem cinema){
        this.getRootPane().setDefaultButton(loginButton);
        this.cinema = cinema;
        this.listeners = new ArrayList<>();
        this.setTitle("Login - Odeon Cinema System");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);

        this.panel = new JPanel();
        this.panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        this.loginFailedPanel = new JPanel();
        this.usernamePanel = new JPanel();
        this.passwordPanel = new JPanel();
        this.buttonPanel = new JPanel();

        this.loginFailedLabel = new JLabel("");
        this.usernameLabel = new JLabel("Username: ");
        this.usernameField = new JTextField(15);
        this.passwordLabel = new JLabel("Password: ");
        this.passwordField = new JPasswordField(15);
        this.loginButton = new JButton("Login");
        loginButton.setDefaultCapable(true);
        loginButton.addActionListener(new LoginListener());
        this.newCustomerButton = new JButton("New Customer");
        newCustomerButton.addActionListener(new NewCustomerListener());
        TextFieldListener tfl = new TextFieldListener();

        loginFailedPanel.add(loginFailedLabel);

        usernamePanel.add(usernameLabel);
        usernamePanel.add(usernameField);
        usernameField.addKeyListener(tfl);

        passwordPanel.add(passwordLabel);
        passwordPanel.add(passwordField);
        passwordField.addKeyListener(tfl);

        buttonPanel.add(loginButton);
        buttonPanel.add(newCustomerButton);

        panel.add(Box.createVerticalStrut(20));
        panel.add(loginFailedPanel);
        panel.add(usernamePanel);
        panel.add(passwordPanel);
        panel.add(buttonPanel);


        this.getContentPane().add(panel, BorderLayout.CENTER);
        this.setLocationRelativeTo(null);
        this.setSize(300,300);
    }

    void setError(String string){
        loginFailedLabel.setText(string);
    }

    void addEventListener(LoginUIListener listener){
        listeners.add(listener);
    }


    class LoginListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                cinema.login(usernameField.getText(), passwordField.getPassword());
                passwordField.setText("");
                usernameField.setText("");
                loginFailedLabel.setText("");
                LoginUI.this.setVisible(false);
                LoginUI.this.listeners.forEach(o-> o.loggedIn(cinema.getAuthenticatedUser()));
            } catch (InvalidLoginException ex){
                passwordField.setText("");
                LoginUI.this.loginFailedLabel.setText("Login Failed: " + ex.getMessage());
                return;
            }
        }
    }

    class TextFieldListener implements KeyListener{
        @Override
        public void keyReleased(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER){
                loginButton.doClick();
            }
        }

        @Override
        public void keyTyped(KeyEvent e) {

        }

        @Override
        public void keyPressed(KeyEvent e) {

        }
    }
    class NewCustomerListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            usernameField.setText("");
            passwordField.setText("");
            LoginUI.this.listeners.forEach(o->o.newCustomerPressed(LoginUI.this));
        }
    }
}
