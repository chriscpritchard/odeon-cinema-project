package uk.co.christopherpritchard.odeoncinemaprojectGUI;

import uk.co.christopherpritchard.odeoncinemaproject.Booking;
import uk.co.christopherpritchard.odeoncinemaproject.Customer;
import uk.co.christopherpritchard.odeoncinemaproject.Film;
import uk.co.christopherpritchard.odeoncinemaproject.Screen;

import javax.swing.*;

public interface MainUIPanelListener {
    void mainUIPanelLoggedOut();
    void newBooking();
    void customerUIAddModifyReview(Film film);
    void uiBack(JPanel panel);
    void futureBookings();
    void pastBookings();
    void futureBookings(Customer customer);
    void pastBookings(Customer customer);
    void modifyBooking(Booking booking, JPanel panel, Customer customer);
    void modifyCustomer();
    void modifyManager();
    void newBooking(Customer customer);
    void filmReview(Film film);
    void addFilm();
    void addShowing();
    void addShowing(Film film);
    void addShowing(Screen screen);
    void addManager();
}
