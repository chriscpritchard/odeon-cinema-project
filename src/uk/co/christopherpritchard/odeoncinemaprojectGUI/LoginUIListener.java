package uk.co.christopherpritchard.odeoncinemaprojectGUI;

import uk.co.christopherpritchard.odeoncinemaproject.User;

import javax.swing.*;


/**
 * an interface to handle events from the LoginUI Class
 */
public interface LoginUIListener {
    void loggedIn(User u);
    void newCustomerPressed(JFrame frame);
}
