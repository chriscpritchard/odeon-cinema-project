package uk.co.christopherpritchard.odeoncinemaproject;

/**
 * An exception thrown if the booking fails
 */
public class BookingFailedException extends Exception {

    BookingFailedException(String message){
        super(message);
    }

}
