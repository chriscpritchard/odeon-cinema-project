package uk.co.christopherpritchard.odeoncinemaproject;

public class UserAddFailedException extends Exception {
    public UserAddFailedException(String message){
        super(message);
    }
}
