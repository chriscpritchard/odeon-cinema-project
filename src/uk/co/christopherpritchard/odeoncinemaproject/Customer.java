package uk.co.christopherpritchard.odeoncinemaproject;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;


/**
 * The Customer Class
 * @author Chris Pritchard
 * @version 2017-11-13
 */
public class Customer extends User{

    private Set<Booking> bookings;
    private Set<Review> reviews;
    private final String type = "customer";

    @Override
    public String getType() {
        return type;
    }

    Customer(String username, char[] password, String firstname, String lastname){
        super(username, password, firstname, lastname, EnumSet.of(
                Permissions.ADD_BOOKING_SELF,
                Permissions.ADD_REVIEW,
                Permissions.CHANGE_BOOKING,
                Permissions.LIST_FILMS,
                Permissions.LIST_SCREENS,
                Permissions.LIST_SHOWINGS,
                Permissions.SHOW_REVIEW
        ));
        this.bookings = new HashSet<>();
        this.reviews = new HashSet<>();
    }

    void addBooking(Booking book){
        bookings.add(book);
    }

    void removeBooking(Booking book){
        bookings.remove(book);
    }

    void addReview(Review review) throws IllegalArgumentException{
        Set<Review> duplicateFilms = new HashSet<>();
        reviews.forEach(o->{
        if (o.getFilm() == review.getFilm()){
              duplicateFilms.add(o);
        }
        });
        reviews.removeAll(duplicateFilms);
        reviews.add(review);
    }

    void addReviewAnyway(Review review){
        Set<Review> duplicateFilms = new HashSet<>();
        reviews.forEach(o->{
            if (o.getFilm() == review.getFilm()){
                duplicateFilms.add(o);
            }
        });
        reviews.removeAll(duplicateFilms);
        reviews.add(review);
    }
    public Set<Booking> getBookings() {
        return new HashSet<>(bookings);
    }

    public Set<Booking> getBookings(LocalDate date) {
        return getBookings(date, date );
    }
    public Set<Booking> getBookings(LocalDate start, LocalDate end) {
        Set<Booking> bookingsBetween = new HashSet<>();
        Set<Booking> parallelBookings = Collections.synchronizedSet(bookingsBetween);
        bookings.parallelStream().forEach(o -> {
            if ((o.getShowing().getDatetime().isAfter(start.atStartOfDay()) && (o.getShowing().getDatetime().isBefore(end.plusDays(1).atStartOfDay())))) {
                parallelBookings.add(o);
            }
        });
        return bookingsBetween;
    }

    public Set<Review> getReviews() {
        return new HashSet<>(reviews);
    }

    public Review getReview(Film film){
        List<Review> reviewsOfFilm = new ArrayList<>(reviews.stream().filter(o -> o.getFilm() == film).collect(Collectors.toList()));
        if (reviewsOfFilm.isEmpty()){
            return null;
        }else{
            return reviewsOfFilm.get(0);
        }
    }
}
