package uk.co.christopherpritchard.odeoncinemaproject;

public abstract class Payment {
    private int amount;


    Payment(int amount) {
        this.amount = amount;
    }

    public int getAmount(){
        return amount;
    }

    public abstract String getType();

    abstract boolean wasSuccessful();
}
