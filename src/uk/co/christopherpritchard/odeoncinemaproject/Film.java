package uk.co.christopherpritchard.odeoncinemaproject;

import java.time.Duration;
import java.time.LocalDate;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * The Film Class
 * @author Chris Pritchard
 * @version 2017-11-13
 */
public class Film {
    private String name;
    private Duration duration;
    private Set<Showing> showings;
    private Set<Review> reviews;


    Film(String name, Duration duration) {
        this.name = name;
        this.duration = duration;
        this.showings = new HashSet<>();
        this.reviews = new HashSet<>();
    }

    public String getName() {
        return name;
    }

    public Duration getDuration() {
        return duration;
    }

    void addReview(Review review){
        Set<Review> duplicateCusts = new HashSet<>();
        reviews.forEach(o->{
            if (o.getCustomer() == review.getCustomer()){
                duplicateCusts.add(o);
            }
        });
        reviews.removeAll(duplicateCusts);
        reviews.add(review);
    }

    int getTakings(LocalDate startDate, LocalDate endDate){
        Set<Showing> showingsBetween = this.getShowings(startDate, endDate);
        int takings = 0;
        for (Showing show : showingsBetween){
            takings = takings + show.getTakings();
        }
        return takings;
    }

    public Set<Showing> getShowings(){
        return new HashSet<>(showings);
    }

    public Set<Showing> getShowings(LocalDate date){
        Set<Showing> showingsOn = new HashSet<>();
        Set<Showing> parallelShowings = Collections.synchronizedSet(showingsOn);
        showings.parallelStream().forEach(o->{
            if ((o.getDatetime().isAfter(date.atStartOfDay()) && o.getDatetime().isBefore(date.plusDays(1).atStartOfDay()))){
                parallelShowings.add(o);
            }
        });
        return showingsOn;
    }

    void removeShowing(Showing showing){
        showings.remove(showing);
    }
    public Set<Showing> getShowings(LocalDate startDate, LocalDate endDate){
        Set<Showing> showingsBetween = new HashSet<>();
        Set<Showing> parallelShowings = Collections.synchronizedSet(showingsBetween);
        showings.parallelStream().forEach(o->{
            if ((o.getDatetime().isAfter(startDate.atStartOfDay()) && (o.getDatetime().isBefore(endDate.plusDays(1).atStartOfDay())))){
                parallelShowings.add(o);
            }
        });
        return showingsBetween;
    }

    void addShowing(Showing showing){
        showings.add(showing);
    }

    public int getNumSpectators(LocalDate startDate, LocalDate endDate){
        Set<Showing> showings = getShowings(startDate, endDate);
        int numSpectators = 0;
        for (Showing showing : showings){
            numSpectators = numSpectators + showing.getNumBookings();
        }
        return numSpectators;
    }

    public float getRating(){
        long ratingSum = 0;
        long ratingNum = 0;

        for (Showing showing : showings){
            for (Review review : reviews){
                ratingSum = ratingSum + review.getRating();
                ratingNum++;
            }
        }
            float rating = (float) ratingSum / ratingNum;
        return rating;
    }

    public float getRating(LocalDate startDate, LocalDate endDate){
        Set<Showing> showings = getShowings(startDate, endDate);
        long ratingSum = 0;
        long ratingNum = 0;

        for (Showing showing : showings){
            for (Review review : reviews){
                ratingSum = ratingSum + review.getRating();
                ratingNum++;
            }
        }
        return (float) ratingSum / ratingNum;
    }

    public Set<Review> getReviews() {
        return new HashSet<>(reviews);
    }

    @Override
    public String toString() {
        return this.getName();
    }
}
