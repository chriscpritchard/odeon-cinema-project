package uk.co.christopherpritchard.odeoncinemaproject;

/**
 * A Class representing a seat
 * @author Chris Pritchard
 * @version 2017-11-13
 */
public abstract class Seat {

        /**
     * A boolean value representing the occupied state of the seat
     */
    private boolean occupied;

    /**
     * A constructor for the seat object
     */
    public Seat(){
        this.occupied = false;
    }

    abstract public String getSeatType();

    abstract public int getCostUplift();

    /**
     * A "getter" for the occupied status of the seat
     * @return whether the seat is occupied
     */
    public boolean isOccupied(){
        return occupied;
    }

    /**
     * A method to change whether the seat is occupied or not
     * @param occ the new occupied status of the seat, must be different to the current status of the seat
     * @throws SeatStatusWrongException if the seat status is the same as occ
     */
    public void setOccupied(boolean occ) throws SeatStatusWrongException{
        if(occ == occupied){
            throw new SeatStatusWrongException(occ);
        }
        else {
            occupied = occ;
    }
    }
}
