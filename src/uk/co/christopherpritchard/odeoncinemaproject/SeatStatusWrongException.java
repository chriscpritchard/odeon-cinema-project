package uk.co.christopherpritchard.odeoncinemaproject;

/**
 * An exception thrown if a seat's status is trying to be changed to what it already is
 */
public class SeatStatusWrongException extends Exception {
    /**
     * A boolean representing the status of the seat that was incorrect
     */
    private boolean status;

    /**
     * A constructor for the SeatStatusWrongException
     * @param status the occupied status of the seat in question
     */
    public SeatStatusWrongException(boolean status){
        this.status = status;
    }

    /**
     * gets the status of the seat in question
     * @return the occupied status of the seat that was incorrect
     */
    public boolean getStatus(){
        return this.status;
    }
}
