package uk.co.christopherpritchard.odeoncinemaproject;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * The Screen Class
 * @author Chris Pritchard
 * @version 2017-11-13
 */
public class Screen {
    private int numStandardSeats;
    private int numVIPSeats;
    private Set<Showing> showings;
    private int maxNumShowings;
    private int id;


    Screen(int numStandardSeats, int numVIPSeats, int maxNumShowings, int id) {
        this.numStandardSeats = numStandardSeats;
        this.numVIPSeats = numVIPSeats;
        this.showings = new HashSet<>();
        this.maxNumShowings = maxNumShowings;
        this.id = id;
    }

    public int getNumStandardSeats() {
        return numStandardSeats;
    }

    public int getNumVIPSeats() {
        return numVIPSeats;
    }


    public int getMaxNumShowings() {
        return maxNumShowings;
    }


    public Set<Showing> getShowings(LocalDate date){
        Set<Showing> showingsOn = new HashSet<>();
        Set<Showing> parallelShowings = Collections.synchronizedSet(showingsOn);
        showings.parallelStream().forEach(o->{
            if ((o.getDatetime().isAfter(date.atStartOfDay()) && o.getDatetime().isBefore(date.plusDays(1).atStartOfDay()))){
                parallelShowings.add(o);
            }
        });
        return showingsOn;
    }

    void removeShowing(Showing showing){
        showings.remove(showing);
    }

    public Set<Showing> getShowings(LocalDate startDate, LocalDate endDate){
        Set<Showing> showingsBetween = new HashSet<>();
        Set<Showing> parallelShowings = Collections.synchronizedSet(showingsBetween);
        showings.parallelStream().forEach(o->{
            if ((o.getDatetime().isAfter(startDate.atStartOfDay()) && (o.getDatetime().isBefore(endDate.plusDays(1).atStartOfDay())))){
                parallelShowings.add(o);
            }
        });
        return showingsBetween;
    }

    void addShowing(Showing showing){
        showings.add(showing);
    }

    public int getId() {
        return id;
    }

    public String toString(){
        return "Screen " + id;
    }
}
