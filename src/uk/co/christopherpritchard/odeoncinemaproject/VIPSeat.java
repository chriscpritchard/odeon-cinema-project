package uk.co.christopherpritchard.odeoncinemaproject;

public class VIPSeat extends Seat {
    public static final int UPLIFT = 250;
    public static final String TYPE = "VIP";

    VIPSeat() {
        super();
    }

    @Override
    public String getSeatType() {
        return TYPE;
    }

    @Override
    public int getCostUplift() {
        return UPLIFT;
    }
}
