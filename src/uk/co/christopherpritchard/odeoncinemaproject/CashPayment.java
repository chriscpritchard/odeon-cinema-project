package uk.co.christopherpritchard.odeoncinemaproject;

/**
 * A class representing a cash payment
 */
public class CashPayment extends Payment {

    //final because once assigned a new payment would have to be made if this payment failed
    private final boolean successful;
    public static final String TYPE = "Cash";

    public CashPayment(int amount) {
        super(amount);
        successful = doPayment(amount);
    }

    @Override
    boolean wasSuccessful() {
        return successful;
    }

    @Override
    public String getType() {
        return TYPE;
    }

    /**
     * a private method to do the payment
     * @param amount the amount of the payment
     * @return whether or not the payment succeeded
     */
    private static boolean doPayment(int amount){
        //currently always returns true. Code to integrate with till system would go here.
        return true;
    }
}
