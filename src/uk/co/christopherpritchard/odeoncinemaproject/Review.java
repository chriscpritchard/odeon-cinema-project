package uk.co.christopherpritchard.odeoncinemaproject;

import java.util.ArrayList;

/**
 * The Review Class
 * @author Chris Pritchard
 * @version 2017-11-13
 */
public class Review{
    private Showing showing;
    private byte rating;
    private String review;
    private Customer customer;


    Review(Customer customer, Showing showing, byte rating, String review) throws IllegalArgumentException{
        this.customer = customer;
        this.showing = showing;
        if (rating >= 1 && rating <= 5){
            this.rating = rating;
        } else{
            throw new IllegalArgumentException("Rating must be between 1 and 5");
        }
        this.review = review;
    }


    public Film getFilm() {
        return showing.getFilm();
    }

    public byte getRating() {
        return rating;
    }

    public void setRating(byte rating) throws IllegalArgumentException {
        if (rating >= 1 && rating <= 5){
            this.rating = rating;
        } else{
            throw new IllegalArgumentException("Rating must be between 1 and 5");
        }
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public Customer getCustomer(){
        return customer;
    }

}
