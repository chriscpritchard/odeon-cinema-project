package uk.co.christopherpritchard.odeoncinemaproject;

import java.util.Map;
import java.util.regex.Pattern;

class Validator {
    /**
     * A regex Pattern object to allow for username validation - final as we don't want it changing (allows A-Z, a-z, 0-9 and _)
     */
    private final Pattern usernamePattern = Pattern.compile("[A-Za-z0-9_]+");

    private Map<String, User> users;

    Validator(Map<String, User> users) {
        this.users = users;
    }

    boolean validateUserAdd(String username, String firstName, String lastName, char[] password) throws UserAddFailedException {
        boolean ret = false;
        //checks if user is a duplicate
        if (users.containsKey(username)) {
            throw new UserAddFailedException("Username must be unique");
            //can't have empty names, or names starting with a space
        } else{
            ret = validateUserCommon(username, firstName, lastName, password);
        }
        return ret;

    }

    boolean validateUserChange(User user, String username, String firstName, String lastName, char[] password) throws UserAddFailedException {
        boolean ret = false;
        //checks if user is a duplicate
        if (users.containsKey(username) && (!(user.getUsername().equals(username)))) {
            throw new UserAddFailedException("Username must be unique");
            //can't have empty names, or names starting with a space
        } else{
            ret = validateUserCommon(username, firstName, lastName, password);
        }
        return ret;
    }

    boolean validatePassword(char[] password){
        return (password != null && password.length >= 6 && password.length <= 255);
    }

    boolean validateRealName(String firstName, String lastName){
        return !(firstName.length() > 255 || lastName.length() > 255 || firstName.equals("") || firstName.startsWith(" ") || lastName.equals("") || lastName.startsWith(" "));
    }

    boolean validateUsername(String username){
        return (username != null && username.length() < 256 && usernamePattern.matcher(username).matches());
    }

    boolean validateUserCommon(String username, String firstName, String lastName, char[] password) throws UserAddFailedException{
        boolean ret = false;

        if (!validateRealName(firstName, lastName)) {
            throw new UserAddFailedException("Names must not be blank, start with a space, or be over 255 characters");
            //runs against the username regexp defined at the start
        } else if (!validateUsername(username)) {
            throw new UserAddFailedException("Username must only contain uppercase and lowercase letters, numbers or an underscore, and be under 256 characters");
            //ensures there is a password of at least six characters
        } else if (!validatePassword(password)) {
            throw new UserAddFailedException("Password must be at least 6 characters");
        } else{
            ret = true;
        }
        //noinspection ConstantConditions
        return ret;
    }
}