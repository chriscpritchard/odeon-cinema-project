package uk.co.christopherpritchard.odeoncinemaproject;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * The Showing Class
 * @author Chris Pritchard
 * @version 2017-11-13
 */

public class Showing {

    private Film film;
    private Screen screen;
    private LocalDateTime datetime;
    private Set<Seat> seats;
    private Set<Booking> bookings;
    private Set<Review> reviews;
    private int cost;
    private int takings;

    public Showing(Film film, Screen screen, LocalDateTime datetime, int cost) {
        this.film = film;
        this.seats = new HashSet<>();
        this.bookings = new HashSet<>();
        this.screen = screen;
        this.datetime = datetime;
        this.cost = cost;
        for (int i = 0; i < screen.getNumStandardSeats(); i++){
            seats.add(new StandardSeat());
        }
        for (int i = 0; i < screen.getNumVIPSeats(); i++){
            seats.add(new VIPSeat());
        }
    }

    public Film getFilm() {
        return film;
    }

    public Screen getScreen() {
        return screen;
    }

    public LocalDateTime getDatetime() {
        return datetime;
    }

    public int getCost() {
        return cost;
    }

    public Set<Review> getReviews(){
        return reviews;
    }

    public Set<Booking> getBookings(){
        return bookings;
    }

    public Set<Seat> getUnbookedSeats(){
        Set<Seat> unbookedSeats = new HashSet<>();
        Set<Seat> parallelSeats = Collections.synchronizedSet(unbookedSeats);
        seats.parallelStream().forEach(o->{
            if (!o.isOccupied()){
                parallelSeats.add(o);
            }
        });
        return unbookedSeats;
    }

    public List<Seat> getUnbookedSeats(String seatType){
        List<Seat> unbookedSeats = new ArrayList<>();
        List<Seat> parallelSeats = Collections.synchronizedList(unbookedSeats);
        seats.parallelStream().forEach(o->{
            if (!o.isOccupied() && o.getSeatType() == seatType){
                parallelSeats.add(o);
            }
        });
        return unbookedSeats;
    }


    void addBooking(Booking book) throws BookingFailedException{
        bookings.add(book);
        try{
            book.getSeat().setOccupied(true);
        } catch (SeatStatusWrongException e){
            throw new BookingFailedException("Seat status incorrect: " + e.getMessage());
        }
        takings = takings + book.getCost();
    }

    void removeBooking(Booking book) throws BookingFailedException{
        bookings.remove(book);
        try{
            book.getSeat().setOccupied(false);
        } catch (SeatStatusWrongException e){
            throw new BookingFailedException("Seat status incorrect: " + e.getMessage());
        }
        takings = takings - book.getCost();
    }

    public int getTakings(){
        return takings;
    }

    int getNumBookings(){
        return bookings.size();
    }

    @Override
    public String toString() {
        return this.getDatetime().format(DateTimeFormatter.ofPattern("HH:mm"));
    }
}
