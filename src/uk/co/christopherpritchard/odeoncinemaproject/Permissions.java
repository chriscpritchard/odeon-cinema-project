package uk.co.christopherpritchard.odeoncinemaproject;

/**
 *
 */
public enum Permissions {
    ADD_MANAGER,
    ADD_BOOKING,
    ADD_BOOKING_SELF,
    ADD_REVIEW,
    ADD_REVIEW_MANAGER,
    ADD_SCREEN,
    ADD_SHOWING,
    ADD_FILM,
    CHANGE_BOOKING,
    CHANGE_ALL_BOOKINGS,
    REMOVE_BOOKING,
    SHOW_REVIEW,
    LIST_SCREENS,
    LIST_FILMS,
    LIST_SHOWINGS,
    RUN_REPORTS
}
