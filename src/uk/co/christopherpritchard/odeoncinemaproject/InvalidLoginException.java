package uk.co.christopherpritchard.odeoncinemaproject;

public class InvalidLoginException extends Exception {
    InvalidLoginException(String reason){
        super(reason);
    }
}
