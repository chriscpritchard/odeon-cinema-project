package uk.co.christopherpritchard.odeoncinemaproject;


import java.util.EnumSet;

public class Manager extends User {

    private final String type = "manager";

    /**
     * Default constructor for Manager
     * @param username the username for the manager
     * @param password the password for the manager
     * @param firstname the first name for the manager
     * @param lastname the last name of the manager
     */
    Manager(String username, char[] password, String firstname, String lastname) {
        //Managers have all permissions except for thg ability to add a review
        //They do not need permission to change a booking, as they have permission to change all bookings
        super(username, password, firstname, lastname, EnumSet.of(
                Permissions.ADD_MANAGER,
                Permissions.ADD_BOOKING,
                Permissions.ADD_SCREEN,
                Permissions.ADD_SHOWING,
                Permissions.ADD_FILM,
                Permissions.ADD_REVIEW_MANAGER,
                Permissions.CHANGE_ALL_BOOKINGS,
                Permissions.REMOVE_BOOKING,
                Permissions.SHOW_REVIEW,
                Permissions.LIST_SCREENS,
                Permissions.LIST_FILMS,
                Permissions.LIST_SHOWINGS,
                Permissions.RUN_REPORTS
        ));
    }


    @Override
    public String getType() {
        return type;
    }
}
