package uk.co.christopherpritchard.odeoncinemaproject;

public class StandardSeat extends Seat {
    public static final int UPLIFT = 0;
    public static final String TYPE = "Standard";


    public StandardSeat() {
        super();
    }

    @Override
    public String getSeatType() {
        return TYPE;
    }

    @Override
    public int getCostUplift() {
        return UPLIFT;
    }
}
