package uk.co.christopherpritchard.odeoncinemaproject;

import java.time.format.DateTimeFormatter;

/**
 * A class representing a Booking
 * @author Chris Pritchard
 * @version 1.0
 */
public class Booking{

    private Customer customer;
    private Showing showing;
    private Payment payment;
    private Seat seat;

    Booking(Customer customer, Showing showing, Payment payment, Seat seat) {
        this.customer = customer;
        this.showing = showing;
        this.payment = payment;
        this.seat = seat;
    }

    public Showing getShowing() {
        return showing;
    }

    public Customer getCustomer() {
        return customer;
    }

    int getCost(){
        return payment.getAmount();
    }

    public Seat getSeat(){
        return seat;
    }

    public Payment getPayment(){
        return payment;
    }

    /**
     * an overridden toString method that returns the booking in a human readable format
     * @return A string representing this Booking
     */
    @Override
    public String toString() {
        String sb = String.valueOf(this.getShowing().getFilm()) + ": " +
                this.getShowing().getScreen().toString() + " - " +
                this.getSeat().getSeatType() + " (" +
                this.getShowing().getDatetime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")) + ")";
        return sb;
    }
}
