package uk.co.christopherpritchard.odeoncinemaproject;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Random;
import java.util.Set;

/**
 * A class representing a user within the system
 */
public abstract class User {
    /**
     * The username of the user
     */
    private String username;
    /**
     * the (hashed) password of the user
     */
    private byte[] password;
    /**
     * the salt used for the hashing algorithm
     */
    private final byte[] salt;

    /**
     * the user's first name
     */
    private String firstname;
    /**
     * the user's last name
     */
    private String lastname;
    /**
     * The users permissions
     */
    private Set<Permissions> permissions;


    /**
     * The constructor for a User object
     * @param username the username
     * @param password the password (char[] array instead of string as per Java's guidelines on passwords)
     * @param firstname the first name for the user
     * @param lastname the last name for the user
     * @param permissions the permissions for the user
     */

    User(String username, char[] password, String firstname, String lastname, Set<Permissions> permissions){
        // initialise instance variables (except password)
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.permissions = permissions;

        // initialise the password

        // an RNG to generate the salt - needs to be cryptographically secure so using SecureRandom
        Random rng = new SecureRandom();
        //A 32 byte (256 bit) salt - sufficient for security
        this.salt = new byte[32];
        //generate the salt
        rng.nextBytes(salt);
        //hash the password using the hashing method below
        //use 25000 iterations as it should be secure enough, but also quick enough to authenticate
        this.password = hashPassword(password,this.salt);
    }


    public boolean authenticate(char[] password){
        byte[] hashedPassword = hashPassword(password, this.salt);
        return Arrays.equals(hashedPassword,this.password);
    }

    public void changePassword(char[] oldPassword, char[] newPassword) throws InvalidLoginException{
        if (authenticate(oldPassword)){
            password = hashPassword(newPassword, salt);
        }else{
            throw new InvalidLoginException("Existing password incorrect!");
        }
    }

    public Set<Permissions> getPermissions(){
        return permissions;
    }

    public String getUsername() {
        return username;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    void setUsername(String username) {
        this.username = username;
    }

    void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * A Password hashing method to store passwords securely - from https://www.owasp.org/index.php/Hashing_Java
     * throws runtime exceptions as KeySpec and Algorithms are hardcoded (to ensure weak algorithms aren't used)
     * Using in order to ensure password security as storing passwords in plaintext is insecure, modified to remove "final" from signature as utilising as a private method
     * Utilises the JCE library
     * Licensed under the CC-BY-SA license V4.0 (Open Web Application Security Project, 2016)
     * @param password the password to be hashed
     * @param salt the salt to be used as part of the hashing algorithm
     * @param iterations the number of iterations that the algorithm is run
     * @param keyLength the length of the key used
     * @return the hashed password
     */
    private byte[] hashPassword( final char[] password, final byte[] salt, final int iterations, final int keyLength ){

        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance( "PBKDF2WithHmacSHA512" );
            PBEKeySpec spec = new PBEKeySpec( password, salt, iterations, keyLength );
            SecretKey key = skf.generateSecret( spec );
            byte[] res = key.getEncoded( );
            return res;

        } catch( NoSuchAlgorithmException | InvalidKeySpecException e ) {
            throw new RuntimeException( e );
        }
    }

    @Override
    public String toString() {
        return this.getUsername() + " (" + this.getFirstname() + " " + this.getLastname() + ")";
    }

    /**
     * Password hashing method with defaults for iteration / key lengths
     * @param password the (plantext) password
     * @param salt the salt
     * @return the hashed password
     */
    private byte[] hashPassword( final char[] password, final byte[] salt){
        return hashPassword(password, salt, 25000, 256);
    }

    public abstract String getType();

}
