package uk.co.christopherpritchard.odeoncinemaproject;


import javax.naming.NoPermissionException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Full testing class for the Odeon cinema project
 * @author Chris Pritchard
 * @version 2017-11-13
 **/

public class CinemaTest{
    CinemaBookingSystem bookingSystem;
    boolean interactive;

    public CinemaTest(CinemaBookingSystem bookingSystem) {
        this.bookingSystem = bookingSystem;
        interactive = true;
    }
    public CinemaTest(CinemaBookingSystem bookingSystem, boolean interactive) {
        this(bookingSystem);
        this.interactive = interactive;
    }

    public void doTest(){
        System.out.println("Testing started");
        managerLoginTest();
        pressEnter();
        createDataTest();
        pressEnter();

    }

    private void managerLoginTest(){
        System.out.println("Checking that the cinema system is not logged in");
        System.out.println("Running bookingSystem.getAuthenticatedUser()");
        System.out.println("expected \"null\"");
        System.out.println(bookingSystem.getAuthenticatedUser());
        System.out.println("Running bookingSystem.logout();");
        bookingSystem.logout();
        pressEnter();
        System.out.println("Trying to login with an incorrect username");
        System.out.println("Running bookingSystem.login(\"incorrect\", \"testmanager\".toCharArray());");
        System.out.println("EXPECTED: \"Invalid username or password\"");
        try{
            bookingSystem.login("incorrect", "testmanager".toCharArray());
        } catch (InvalidLoginException e){
            System.out.println(e.getMessage());
        }
        System.out.println("Running bookingSystem.getAuthenticatedUser()");
        System.out.println("EXPECTED: \"null\"");
        System.out.println(bookingSystem.getAuthenticatedUser());
        System.out.println("Running bookingSystem.logout();");
        bookingSystem.logout();
        pressEnter();
        System.out.println("Trying to login with an incorrect password");
        System.out.println("Running bookingSystem.login(\"testmanager\", \"incorrect\".toCharArray());");
        System.out.println("EXPECTED: \"Invalid username or password\"");
        try{
            bookingSystem.login("testmanager", "incorrect".toCharArray());
        } catch (InvalidLoginException e){
            System.out.println(e.getMessage());
        }
        System.out.println("Running bookingSystem.getAuthenticatedUser()");
        System.out.println("EXPECTED: \"null\"");
        System.out.println(bookingSystem.getAuthenticatedUser());
        System.out.println("Running bookingSystem.logout();");
        bookingSystem.logout();
        pressEnter();
        System.out.println("Trying to login with correct manager details");
        System.out.println("Running bookingSystem.login(\"testmanager\", \"testmanager\".toCharArray());");
        try{
            bookingSystem.login("testmanager", "testmanager".toCharArray());
        } catch (InvalidLoginException e){
            System.out.println(e.getMessage());
        }
        System.out.println("Running bookingSystem.getAuthenticatedUser()");
        System.out.println("EXPECTED: \"testmanager (Default Manager)\"");
        System.out.println(bookingSystem.getAuthenticatedUser());
    }

    private void createDataTest(){
        System.out.println("Testing data creation");
        System.out.println("Creating films");
        try {
            System.out.println("Running bookingSystem.addFilm(\"Die Hard\", Duration.ofMinutes((long) 150))");
            bookingSystem.addFilm("Die Hard", Duration.ofMinutes((long) 150));
            System.out.println("Running bookingSystem.addFilm(\"Die Hard 2\", Duration.ofMinutes((long) 150));");
            bookingSystem.addFilm("Die Hard 2", Duration.ofMinutes((long) 150));
            bookingSystem.addFilm("Taken", Duration.ofMinutes((long) 150));
            bookingSystem.addFilm("Blade Runner", Duration.ofMinutes((long) 150));
            bookingSystem.addFilm("Doom", Duration.ofMinutes((long) 150));
        }catch (NoPermissionException | IllegalArgumentException ex){
            //shouldn't happen
            System.err.println("ERROR: " + ex.getMessage());
        }
        pressEnter();
        System.out.println("Creating duplicate film");
        System.out.println("Running bookingSystem.addFilm(\"Die Hard\", Duration.ofMinutes((long) 150))");
        System.out.println("EXPECTED: The film name must be unique!");
        try {
            bookingSystem.addFilm("Die Hard", Duration.ofMinutes((long) 150));
        }catch (NoPermissionException | IllegalArgumentException ex){
            System.out.println(ex.getMessage());
        }
        pressEnter();
        System.out.println("Creating film with no name");
        System.out.println("Running bookingSystem.addFilm(\"\", Duration.ofMinutes((long) 150))");
        System.out.println("EXPECTED: The film name must not be blank!");
        try {
            bookingSystem.addFilm("", Duration.ofMinutes((long) 150));
        }catch (NoPermissionException | IllegalArgumentException ex){
            System.out.println(ex.getMessage());
        }
        pressEnter();
        System.out.println("Creating film with no duration");
        System.out.println("Running bookingSystem.addFilm(\"Die Hard 3\", Duration.ofMinutes((long) 0))");
        System.out.println("EXPECTED: The film duration must be greater than 0!");
        try {
            bookingSystem.addFilm("Die Hard 3", Duration.ofMinutes((long) 0));
        }catch (NoPermissionException | IllegalArgumentException ex){
            System.out.println(ex.getMessage());
        }
        pressEnter();
        System.out.println("Creating film with negative duration");
        System.out.println("Running bookingSystem.addFilm(\"Die Hard 4\", Duration.ofMinutes((long) -10))");
        System.out.println("EXPECTED: The film duration must be greater than 0!");
        try {
            bookingSystem.addFilm("Die Hard 4", Duration.ofMinutes((long) -10));
        }catch (NoPermissionException | IllegalArgumentException ex){
            System.out.println(ex.getMessage());
        }
        pressEnter();
        System.out.println("Testing whilst logged out");
        bookingSystem.logout();
        System.out.println("Creating film whilst logged out");
        System.out.println("Running bookingSystem.addFilm(\"Die Hard 4\", Duration.ofMinutes((long) 172))");
        System.out.println("EXPECTED: Sorry, you do not have the ADD_FILM Permission");
        try {
            bookingSystem.addFilm("Die Hard 4", Duration.ofMinutes((long) -10));
        }catch (NoPermissionException | IllegalArgumentException ex){
            System.out.println(ex.getMessage());
        }
        try{
            bookingSystem.login("testmanager", "testmanager".toCharArray());
        } catch (InvalidLoginException e){
            System.err.println(e.getMessage());
        }
        pressEnter();
        System.out.println("Printing the films in the system");
        List<Film> films = new ArrayList<>(bookingSystem.getFilms());
        films.sort(Comparator.comparing(Film::getName));
        for (Film film: films){
            System.out.println(film);
        }
        System.out.println("Adding customers...");
        System.out.println("Running bookingSystem.addCustomer(\"customer1\", \"Default\", \"Customer\", \"customer\".toCharArray());");
        System.out.println("to");
        System.out.println("bookingSystem.addCustomer(\"customer51\", \"Default\", \"Customer\", \"customer\".toCharArray());");
        try {
            for (int i = 1; i <= 51; i++) {
                String s = String.format("%02d", i);
                bookingSystem.addCustomer("customer"+s, "Default", "Customer", "customer".toCharArray());
            }
        } catch (Exception e){
            System.err.println("Error: " + e.getMessage());
        }
        pressEnter();
        System.out.println("Printing the users in the system");
        List<Customer> custs = new ArrayList<>(bookingSystem.getCustomers());
        custs.sort(Comparator.comparing(User::getUsername));
        for (Customer customer : custs){
            System.out.println(customer);
        }
        pressEnter();
        System.out.println("Creating showings");
        try{
            for (Screen screen: bookingSystem.getScreens()) {
                bookingSystem.addShowingAnyway(bookingSystem.getFilm("Die Hard"), screen, LocalDateTime.of(LocalDate.now().plusDays(1), LocalTime.of(14, 00, 00)), 600);
                bookingSystem.addShowingAnyway(bookingSystem.getFilm("Die Hard 2"), screen, LocalDateTime.of(LocalDate.now().plusDays(1), LocalTime.of(17, 00, 00)), 700);
                bookingSystem.addShowingAnyway(bookingSystem.getFilm("Taken"), screen, LocalDateTime.of(LocalDate.now().plusDays(1), LocalTime.of(20, 00, 00)), 600);
                bookingSystem.addShowingAnyway(bookingSystem.getFilm("Blade Runner"), screen, LocalDateTime.of(LocalDate.now().plusDays(1), LocalTime.of(23, 00, 00)), 700);
                bookingSystem.addShowingAnyway(bookingSystem.getFilm("Die Hard 2"), screen, LocalDateTime.of(LocalDate.now().plusDays(2), LocalTime.of(17, 00, 00)), 700);
                bookingSystem.addShowingAnyway(bookingSystem.getFilm("Taken"), screen, LocalDateTime.of(LocalDate.now().plusDays(2), LocalTime.of(20, 00, 00)), 600);
                bookingSystem.addShowingAnyway(bookingSystem.getFilm("Blade Runner"), screen, LocalDateTime.of(LocalDate.now().plusDays(2), LocalTime.of(23, 00, 00)), 700);
            }
        } catch (Exception e){
            System.err.println("ERROR: " + e.getMessage());
        }
        List<Showing> showings = new ArrayList<>(bookingSystem.getShowings(LocalDate.now().plusDays(1)));
        showings.sort(new ShowingComparator());
        System.out.println("Printing showings created for tomorrow");
        showings.forEach(o-> System.out.println(o.getFilm().getName()+" " + o.getScreen().toString()+" " +o.getDatetime()));
        showings = new ArrayList<>(bookingSystem.getShowings("Taken", LocalDate.now().plusDays(2)));
        System.out.println("Printing showings created for 2 days in the future for \"Taken\"");
        showings.sort(new ShowingComparator());
        showings.forEach(o-> System.out.println(o.getFilm().getName()+" " + o.getScreen().toString()+" " +o.getDatetime()));
        System.out.println("Printing price for showings and invalid seat type");
        try{
            bookingSystem.getPrice(showings.get(0),"INVALID");
        } catch (IllegalArgumentException ex){
            System.out.println("Expected Exception: " + ex.getMessage());
        }
        System.out.println(bookingSystem.getPrice(showings.get(0),"VIP"));
        System.out.println(bookingSystem.getPrice(showings.get(0),"Standard"));
        try{
            bookingSystem.getFilm("This does not exist");
        } catch (IllegalArgumentException ex){
            System.out.println("Expected Exception: " + ex.getMessage());
        }
        try{
            bookingSystem.getShowings("This does not exist", LocalDate.now().plusDays(1));
        } catch (IllegalArgumentException ex){
            System.out.println("Expected Exception: " + ex.getMessage());
        }
        System.out.println("Trying methods whilst logged out");
        bookingSystem.logout();
        try {
            bookingSystem.addManager("test", "Test", "Test", "123456".toCharArray());
        } catch (NoPermissionException ex){
            System.out.println("Expected Exception: " + ex.getMessage());
        } catch (Exception ez){
            System.err.println("NOT EXPECTED EXCEPTION!");
        }
        try {
            Showing show = new ArrayList<>(bookingSystem.getShowings(LocalDate.now().plusDays(1))).get(0);
            bookingSystem.addBooking(show, "Standard", new ArrayList<>(bookingSystem.getCustomers()).get(0),new CardPayment(bookingSystem.getPrice(show, "Standard")));
        } catch (NoPermissionException ex){
            System.out.println("Expected Exception: " + ex.getMessage());
        } catch (Exception ez){
            System.err.println("NOT EXPECTED EXCEPTION!");
        }
        try {
            Showing show = new ArrayList<>(bookingSystem.getShowings(LocalDate.now().plusDays(1))).get(0);
            bookingSystem.addBookingAnyway(show, "Standard", new ArrayList<>(bookingSystem.getCustomers()).get(0),new CardPayment(bookingSystem.getPrice(show, "Standard")));
        } catch (NoPermissionException ex){
            System.out.println("Expected Exception: " + ex.getMessage());
        } catch (Exception ez){
            System.err.println("NOT EXPECTED EXCEPTION!");
        }
    }


    private void pressEnter(){
        if (interactive) {
            System.out.println("Press enter key to continue the test...");
            try {
                System.in.read();
                int count = System.in.available();
                System.in.skip(count);
            } catch (Exception e) {

            }
        }
    }

    private class ShowingComparator implements Comparator<Showing>{
        @Override
        public int compare(Showing o1, Showing o2) {
            int comp = o1.getFilm().getName().compareTo(o2.getFilm().getName());
            if (comp == 0){
                comp = o1.getDatetime().compareTo(o2.getDatetime());
            }
            if (comp == 0){
                comp = o1.getScreen().getId() - o2.getScreen().getId();
            }
            return comp;
        }
    }
}
