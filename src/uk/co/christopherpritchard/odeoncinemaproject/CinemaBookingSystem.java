package uk.co.christopherpritchard.odeoncinemaproject;


import javax.naming.NoPermissionException;
import java.time.*;
import java.util.List;
import java.util.Set;

/**
 * An interface for the cinema booking system
 */
public interface CinemaBookingSystem{

    /**
     * Logs into the system
     * @param username the username to login as
     * @param Password the password to login with
     * @throws InvalidLoginException if the username or password is incorrect
     */
    void login(String username, char[] Password) throws InvalidLoginException;

    /**
     * Logs out of the system
     */
    void logout();

    /**
     * Adds a customer to the system
     * @param username the username of the customer
     * @param firstName the first name of the customer
     * @param lastName the last name of the customer
     * @param password the customer's password
     * @throws UserAddFailedException if adding the user failed (for example, username already exists)
     */
    void addCustomer(String username, String firstName, String lastName, char[] password) throws UserAddFailedException;

    /**
     * Adds a manager to the system
     * @param username the username of the manager
     * @param firstName the first name of the manager
     * @param lastName the last name of the manager
     * @param password the password of the manager
     * @throws UserAddFailedException if adding the user failed (for example, username already exists)
     * @throws NoPermissionException if the User does not have permission to undertake this action
     */
    void addManager(String username, String firstName, String lastName, char[] password) throws UserAddFailedException, NoPermissionException;

    /**
     * adds a new booking
     * @param showing the showing that the booking is for
     * @param seatType the selected seat
     * @param customer the customer the booking is for
     * @param payment the payment for the booking
     * @throws NoPermissionException if the User does not have permission to undertake this action
     * @throws BookingFailedException an exception returned if the booking fails (for example, if the seat is already taken)
     */
    void addBooking(Showing showing, String seatType, Customer customer, Payment payment) throws BookingFailedException, NoPermissionException;

    /**
     * adds a new booking, without validating the date (so can create a booking in the past)
     * @param showing the showing that the booking is for
     * @param seatType the selected seat
     * @param customer the customer the booking is for
     * @param payment the payment for the booking
     * @throws NoPermissionException if the User does not have permission to undertake this action
     * @throws BookingFailedException an exception returned if the booking fails (for example, if the seat is already taken)
     */
    void addBookingAnyway(Showing showing, String seatType, Customer customer, Payment payment) throws BookingFailedException, NoPermissionException;

    /**
     * gets the customers within the system
     * @return a set of all the customers in the system
     */
    Set<Customer> getCustomers();

    /**
     * issue a refund
     * @param seatType the seat type
     * @param showing the showing the refund is for
     * @param paymentType the type of payment
     * @return the Payment object
     * @throws IllegalArgumentException if an argument is invalid
     */
    Payment refund(String seatType, Showing showing, String paymentType) throws IllegalArgumentException;


    /**
     * adds a review
     * @param booking the booking the review is for
     * @param rating the rating the customer gives the film
     * @param review the text of the review
     * @throws NoPermissionException if the User does not have permission to undertake this action
     * @throws IllegalArgumentException if the rating is invalid (i.e. not between 1 and 5)
     */
    void addReview(Booking booking, byte rating, String review) throws IllegalArgumentException, NoPermissionException;

    /**
     * adds a review
     * @param booking the booking the review is for
     * @param rating the rating the customer gives the film
     * @param review the text of the review
     * @param customer the customer the review belongs to
     * @throws NoPermissionException if the User does not have permission to undertake this action
     * @throws IllegalArgumentException if the rating is invalid (i.e. not between 1 and 5)
     */
    void addReview(Booking booking, byte rating, String review, Customer customer) throws IllegalArgumentException, NoPermissionException;

    /**
     * adds a showing (does not check if the showing is in the past)
     * @param film the film to be shown
     * @param screen the screen the film is in
     * @param time the time of the showing
     * @param cost the cost of the showing (in pence)
     * @throws NoPermissionException if the User does not have permission to undertake this action
     * @throws IllegalArgumentException if any of the arguments are invalid
     */
    void addShowingAnyway(Film film, Screen screen, LocalDateTime time, int cost) throws NoPermissionException, IllegalArgumentException;

    /**
     * adds a showing
     * @param film the film to be shown
     * @param screen the screen the film is in
     * @param time the time of the showing
     * @param cost the cost of the showing (in pence)
     * @throws NoPermissionException if the User does not have permission to undertake this action
     * @throws IllegalArgumentException if any of the arguments are invalid
     */
    void addShowing(Film film, Screen screen, LocalDateTime time) throws NoPermissionException, IllegalArgumentException;

    /**
     * Removes a showing
     * @param showing the showing to remove
     * @throws NoPermissionException if the User does not have permission to undertake this action
     * @throws IllegalArgumentException if any of the arguments are invalid
     */
    void removeShowing(Showing showing) throws NoPermissionException, IllegalArgumentException;

    /**
     * adds a film to the system
     * @param name the name of the film
     * @param duration the Duration of the film
     * @throws NoPermissionException if the User does not have permission to undertake this action
     */
    void addFilm(String name, Duration duration) throws NoPermissionException, IllegalArgumentException;

    /**
     * Changes a user
     * @param user the user to change
     * @param username the new username
     * @param firstName the new first name
     * @param lastName the new last name
     * @param newPassword the new password
     * @param oldPassword the old password
     * @throws InvalidLoginException if the old password is incorrect
     * @throws UserAddFailedException if this method fails to change the user
     */
    void changeUser(User user, String username, String firstName, String lastName, char[] newPassword, char[] oldPassword) throws InvalidLoginException, UserAddFailedException;

    /**
     * Changes a user
     * @param user the user to change
     * @param username the new username
     * @param firstName the new first name
     * @param lastName the new last name
     * @param oldPassword the old password
     * @throws InvalidLoginException if the old password is incorrect
     * @throws UserAddFailedException if this method fails to change the user
     */
    void changeUser(User user, String username, String firstName, String lastName, char[] oldPassword) throws UserAddFailedException;

    /**
     * Remove an existing booking (refund goes to the existing payment method)
     * @param booking the booking to be removed
     * @throws NoPermissionException if the User does not have permission to undertake this action
     */
    void removeBooking(Booking booking) throws NoPermissionException, BookingFailedException;

    /**
     * Runs the spectator report
     * @param start the date to start with
     * @param end the date to end with
     * @return A string containing the spectator report
     * @throws NoPermissionException if the user does not have permission to undertake this action
     */
    String runSpectatorReport(LocalDate start, LocalDate end) throws NoPermissionException, IllegalArgumentException;

    /**
     * runs the highest grossing report
     * @param start the date to start with
     * @param end the date to end with
     * @return A string containing the highest grossing report
     * @throws NoPermissionException if the user does not have permission to undertake this action
     */
    String runGrossReport(LocalDate start, LocalDate end) throws NoPermissionException, IllegalArgumentException;


    /**
     * get all the films in this cinema
     * @return a Set of films
     */
    Set<Film> getFilms();

    /**
     * Get the films with a showing on a specific date
     * @param date the date
     * @return a set of films
     */
    Set<Film> getFilms(LocalDate date);

    /**
     * gets the screen's number
     * @param screen the screen in question
     * @return the screen's number
     */
    int getScreenNumber(Screen screen);

    /**
     * Gets the screen number for a specific showing
     * @param show the showing in question
     * @return the screen number
     */
    int getScreenNumber(Showing show);

    /**
     * Gets a film
     * @param exactName the name of the film
     * @return a film (or null) with the name of exactName
     */
    Film getFilm(String exactName);

    /**
     * Converts an int into a String representing a currency
     * @param money the amount
     * @param symbol the prefix
     * @return a String representing the currency (e.g. toDecimalCurrency(152, "£") will return "£1.52")
     */
    String toDecimalCurrency(int money, String symbol);

    /**
     * Gets the price of a specific showing and seat type
     * @param show the showing
     * @param seatType the seat type
     * @return the cost in pence
     * @throws IllegalArgumentException if any of the arguments are invalid
     */
    int getPrice(Showing show, String seatType) throws IllegalArgumentException;

    /**
     * Gets the currently authenticated user
     * @return the currently authenticated user
     */
    User getAuthenticatedUser();

    /**
     * Gets a list of screens in the system
     * @return a list of screens in the system
     */
    List<Screen> getScreens();

    Set<Showing> getShowings(String filmName, LocalDate date) throws IllegalArgumentException;

    Set<Showing> getShowings(LocalDate date);

    Payment pay(String seatType, Showing showing, String paymentType) throws IllegalArgumentException;

}
