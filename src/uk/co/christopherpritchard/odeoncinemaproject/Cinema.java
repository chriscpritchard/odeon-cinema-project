package uk.co.christopherpritchard.odeoncinemaproject;

import uk.co.christopherpritchard.CollectionTools.MapToCollection;

import javax.naming.NoPermissionException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.regex.Pattern;

/**
 * A Class representing a Cinema
 * @author Chris Pritchard
 * @version 2017-11-13
 */
public class Cinema implements CinemaBookingSystem{
    /**
     * A regex Pattern object to allow for username validation - final as we don't want it changing (allows A-Z, a-z, 0-9 and _)
     */
    private final Pattern usernamePattern = Pattern.compile("[A-Za-z0-9_]+");
    /**
     * The currently authenticated user
     */
    private User authenticatedUser;

    /**
     * A map of usernames (String):User objects
     */
    private Map<String, User> users;

    /**
     * A List of the screens - using the position in the list(+1) as "screen number"
     * e.g screens.get(0) would be screen 1
     */
    private List<Screen> screens;
    /**
     * A Map of films, the key being the name of the film
     */
    private Map<String, Film> films;

    /**
     * A set of bookings within the system
     */
    private Set<Booking> bookings;

    /**
     * A set of refunds that have been issued
     */
    private Set<Payment> refunds;

    /**
     * A set of showings within the system
     */
    private Set<Showing> showings;

    /**
     * The number of showings per screen
     */
    private int numShowingsPerScreen;

    /**
     * The validator object
     */
    private Validator validator;


    /**
     * A Constructor for the Cinema class - uses the default values (specified in the assignment)
     * @param managerUsername the username for the first manager to be created
     * @param managerPassword the password for the first manager to be created
     */
    public Cinema(String managerUsername, char[] managerPassword){
        this(6, 4, 40, 10, managerUsername, managerPassword);
    }


    /**
     * A Constructor for the cinema class - allows for custom values to be specified
     * @param numScreens the number of screens this cinema has
     * @param defaultNumShowingsPerScreen the number of showings per screen (can be changed on a screen by screen basis)
     * @param numStandardSeatsPerScreen the number of standard (non-VIP) seats per screen
     * @param numVIPSeatsPerScreen the number of VIP seats per screen
     * @param managerUsername the username for the first manager to be created
     * @param managerPassword the password for the first manager to be created
     */
    public Cinema(int numScreens, int defaultNumShowingsPerScreen, int numStandardSeatsPerScreen, int  numVIPSeatsPerScreen, String managerUsername, char[] managerPassword){
        //initialise instance variables
        this.screens = new ArrayList<>(numScreens);
        this.users = new HashMap<>();
        this.films = new HashMap<>();
        this.bookings = new HashSet<>();
        this.refunds = new HashSet<>();
        this.showings = new HashSet<>();
        this.validator = new Validator(this.users);

        this.numShowingsPerScreen = defaultNumShowingsPerScreen;

        //create the initial manager user - need to do it manually as we don't have a user to check permissions against yet
        users.put(managerUsername, new Manager(managerUsername, managerPassword, "Default", "Manager"));


        //create the screens
        for(int i = 0; i<numScreens; i++){
            screens.add(new Screen(numStandardSeatsPerScreen, numVIPSeatsPerScreen, defaultNumShowingsPerScreen, i+1));
        }
    }

    @Override
    public Set<Film> getFilms() {
        MapToCollection<String, Film> filmMapToCollection = new MapToCollection<>(films);
        return filmMapToCollection.getSet();
    }

    @Override
    public Set<Film> getFilms(LocalDate date) {
        Set<Film> filteredFilms = new HashSet<>();
        Set<Film> parallelFilms = Collections.synchronizedSet(filteredFilms);
        films.entrySet().parallelStream().forEach(e -> {
            if (!e.getValue().getShowings(date).isEmpty()){
                parallelFilms.add(e.getValue());
            }
        } );
        return filteredFilms;
    }

    @Override
    public void login(final String username, final char[] password) throws InvalidLoginException {
        //retrieves the User object from the map
        User user = users.get(username);
        //checks to see if the user is null, and if not calls the authenticate method, if it returns true, the user is authenticated
        if (!(user == null) && user.authenticate(password)){
            this.authenticatedUser = user;
        }
        else{
            //if the user is not authenticated throws an InvalidLoginException which the caller can deal with
            throw new InvalidLoginException("Invalid username or password");
        }
    }

    @Override
    public void logout(){
        //sets authenticatedUser to null on logout
        this.authenticatedUser = null;
    }

    @Override
    public void addCustomer(String username, String firstName, String lastName, char[] password) throws UserAddFailedException {
        if (validator.validateUserAdd(username, firstName, lastName, password)){
            users.put(username, new Customer(username, password, firstName, lastName));
        }
    }

    @Override
    public void changeUser(User user, String username, String firstName, String lastName, char[] newPassword, char[] oldPassword) throws InvalidLoginException, UserAddFailedException {
        if (validator.validateUserChange(user, username, firstName, lastName, newPassword) && user.authenticate(oldPassword)){
            user.setUsername(username);
            user.setFirstname(firstName);
            user.setLastname(lastName);
            user.changePassword(oldPassword, newPassword);
        }
    }

    @Override
    public void changeUser(User user, String username, String firstName, String lastName, char[] oldPassword) throws UserAddFailedException {
        if (validator.validateUserChange(user, username, firstName, lastName, "apasswordthatisdefinatelyvalid".toCharArray())){
            user.setUsername(username);
            user.setFirstname(firstName);
            user.setLastname(lastName);
        }
    }

    @Override
    public List<Screen> getScreens(){
        return new ArrayList<>(screens);
    }

    @Override
    public void addManager(String username, String firstName, String lastName, char[] password) throws UserAddFailedException, NoPermissionException {
        Permissions perm = Permissions.ADD_MANAGER;
        if (validatePermission(perm)){
            if (validator.validateUserAdd(username, firstName, lastName, password)) {
                users.put(username, new Manager(username, password, firstName, lastName));
            }
        }else{
            throw new NoPermissionException(permExceptionString(perm));
        }
    }

    @Override
    public Payment pay(String seatType, Showing showing, String paymentType) throws IllegalArgumentException{
        int cost;
        cost= getPrice(showing, seatType);
        if (paymentType.contentEquals("Card")){
            return new CardPayment(cost);
        }else if (paymentType.contentEquals("Cash")){
            return new CashPayment(cost);
        }else{
            throw new IllegalArgumentException("Payment type does not exist!");
        }
    }

    @Override
    public Payment refund(String seatType, Showing showing, String paymentType) throws IllegalArgumentException{
        int cost;
        Payment ref;
        cost= getPrice(showing, seatType);
        if (paymentType.contentEquals("Card")){
            ref = new CardPayment(-cost);
            refunds.add(ref);
        }else if (paymentType.contentEquals("Cash")){
            ref =  new CashPayment(-cost);
            refunds.add(ref);
        }else{
            throw new IllegalArgumentException("Payment type does not exist!");
        }
        return ref;
    }

    @Override
    public void addBookingAnyway(Showing showing, String seatType, Customer customer, Payment payment) throws BookingFailedException, NoPermissionException {
        Permissions perm = Permissions.ADD_BOOKING;
        if (validatePermission(perm)){
            if (showing.getUnbookedSeats(seatType).isEmpty()){
                throw new BookingFailedException("There are no seats of type:" + seatType);
            }else {
                Seat seat = showing.getUnbookedSeats(seatType).get(0);
                doAddBooking(showing, seat, customer, payment);
            }
        }else{
            throw new NoPermissionException(permExceptionString(perm));
        }
    }

    @Override
    public void addBooking(Showing showing, String seatType, Customer customer, Payment payment) throws BookingFailedException, NoPermissionException {
        Permissions perm = Permissions.ADD_BOOKING;
        Permissions perm2 = Permissions.ADD_BOOKING_SELF;
        if (showing.getDatetime().isBefore(LocalDateTime.now())){
            throw new BookingFailedException("You can't book onto a showing in the past!");
        }
        if (validatePermission(perm) || (validatePermission(perm2) && customer == this.authenticatedUser)){
            if (showing.getUnbookedSeats(seatType).isEmpty()){
                throw new BookingFailedException("There are no seats of type:" + seatType);
            }else {
                Seat seat = showing.getUnbookedSeats(seatType).get(0);
                doAddBooking(showing, seat, customer, payment);
            }
        }else{
            throw new NoPermissionException(permExceptionString(perm));
        }
    }

    @Override
    public void addReview(Booking booking, byte rating, String review) throws IllegalArgumentException, NoPermissionException {
        Permissions perm = Permissions.ADD_REVIEW;
        if (validatePermission(perm) && booking.getCustomer() == this.getAuthenticatedUser()){
            Film film = booking.getShowing().getFilm();
            Customer cust = booking.getCustomer();
            Review rev = new Review(cust, booking.getShowing(), rating, review);
            cust.addReview(rev);
            film.addReview(rev);
        }else{
            throw new NoPermissionException(permExceptionString(perm));
        }
    }

    @Override
    public void addReview(Booking booking, byte rating, String review, Customer customer) throws IllegalArgumentException, NoPermissionException {
        Permissions perm = Permissions.ADD_REVIEW_MANAGER;
        if (validatePermission(perm)){
            Film film = booking.getShowing().getFilm();
            Review rev = new Review(customer, booking.getShowing(), rating, review);
            customer.addReviewAnyway(rev);
            film.addReview(rev);
        }else{
            throw new NoPermissionException(permExceptionString(perm));
        }
    }

    @Override
    public Set<Customer> getCustomers(){
        MapToCollection<String, User> m2c = new MapToCollection<>(users);
        return m2c.getSetOfSubclass(Customer.class);
    }


    @Override
    public void addShowingAnyway(Film film, Screen screen, LocalDateTime time, int cost) throws NoPermissionException, IllegalArgumentException {
        Permissions perm = Permissions.ADD_SHOWING;
        Set<Showing> showings;
        if (validatePermission(perm)){
            showings = screen.getShowings(time.toLocalDate());
            if (showings.size() >= screen.getMaxNumShowings()){
                throw new IllegalArgumentException("Screen already at max number of showings (" + screen.getMaxNumShowings() + ").");
            }else{
                if ( showingClash(showings, time, film)){
                    throw new IllegalArgumentException("Showing cannot occur during another film!");
                } else {
                    Showing showing = new Showing(film, screen, time, cost);
                    screen.addShowing(showing);
                    film.addShowing(showing);
                    showings.add(showing);
                }
            }
        }else{
            throw new NoPermissionException(permExceptionString(perm));
        }
    }
    @Override
    public void addShowing(Film film, Screen screen, LocalDateTime time) throws NoPermissionException, IllegalArgumentException{
        Permissions perm = Permissions.ADD_SHOWING;
        if (validatePermission(perm)){
            if (LocalDateTime.now().isAfter(time)){
                throw new IllegalArgumentException("Showing can't be in the past!");
            }else {
                int cost;
                if (LocalTime.of(time.getHour(), time.getMinute(), time.getSecond()).isBefore(LocalTime.of(17,00))){
                    cost = 600;
                } else if (LocalTime.of(time.getHour(), time.getMinute(), time.getSecond()).isBefore(LocalTime.of(19,00))){
                    cost = 750;
                } else if (LocalTime.of(time.getHour(), time.getMinute(), time.getSecond()).isBefore(LocalTime.of(22,00))){
                    cost = 850;
                } else {
                    cost = 700;
                }
                addShowingAnyway(film,screen,time,cost);
            }
        }else{
            throw new NoPermissionException(permExceptionString(perm));
        }
    }

    @Override
    public void addFilm(String name, Duration duration) throws NoPermissionException, IllegalArgumentException {
        Permissions perm = Permissions.ADD_FILM;
        if (validatePermission(perm)){
            if(films.containsKey(name)){
                throw new IllegalArgumentException("The film name must be unique!");
            }else if (name.isEmpty()) {
                throw new IllegalArgumentException("The film name must not be blank!");
            } else if (duration.isNegative() | duration.isZero()) {
                throw new IllegalArgumentException("The film duration must be greater than 0!");
            } else {
                films.put(name, new Film(name, duration));
            }
        }else{
            throw new NoPermissionException(permExceptionString(perm));
        }
    }

    @Override
    public void removeShowing(Showing showing) throws NoPermissionException, IllegalArgumentException {
        Permissions perm = Permissions.ADD_SHOWING;
        if (validatePermission(perm)){
            if (showing.getDatetime().isAfter(LocalDateTime.now())){
                Film film = showing.getFilm();
                Screen screen = showing.getScreen();
                //Code prior to bug being identifed:
                //Set<Booking> bookings = showing.getBookings();
                //new code:
                List<Booking> bookings = new ArrayList<>(showing.getBookings());
                //The ConcurrentModificationException was being thrown because the removeBooking method modified the set that was being iterated through
                //The fix is to create a collection (in this case a list, as it is fastest for iterating) containing all of the objects from the set
                try {
                    for (Booking booking : bookings) {
                        this.removeBooking(booking);
                    }
                    film.removeShowing(showing);
                    screen.removeShowing(showing);
                }catch (BookingFailedException ex){
                    throw new IllegalArgumentException(ex.getMessage());
                }
            } else {
                throw new IllegalArgumentException("Unable to remove a showing in the past");
            }
        }else{
            throw new NoPermissionException(permExceptionString(perm));
        }
    }


    @Override
    public void removeBooking(Booking booking) throws NoPermissionException, BookingFailedException {
        Permissions perm = Permissions.ADD_BOOKING;
        Permissions perm2 = Permissions.ADD_BOOKING_SELF;
        if (booking.getShowing().getDatetime().isBefore(LocalDateTime.now())){
            throw new BookingFailedException("You can't remove a booking in the past!");
        }
        if (validatePermission(perm) || (validatePermission(perm2) && booking.getCustomer() == this.authenticatedUser)){
            refund(booking.getSeat().getSeatType(), booking.getShowing(), booking.getPayment().getType());
            bookings.remove(booking);
            booking.getShowing().removeBooking(booking);
            booking.getCustomer().removeBooking(booking);
        }else{
            throw new NoPermissionException(permExceptionString(perm));
        }
    }

    @Override
    public int getScreenNumber(Showing show){
        if (show != null) {
            return getScreenNumber(show.getScreen());
        }else{
            return -1;
        }
    }

    @Override
    public int getScreenNumber(Screen screen){
        return screens.indexOf(screen)+1;
    }

    @Override
    public String runSpectatorReport(LocalDate start, LocalDate end) throws NoPermissionException {
        Permissions perm = Permissions.RUN_REPORTS;
        if (validatePermission(perm)) {
            if (end.isAfter(start.minusDays(1))){
                MapToCollection<String, Film> m2c = new MapToCollection<>(this.films);
                List<Film> filmList = m2c.getSortedList(Comparator.comparingInt(o -> o.getNumSpectators(start, end)));
                StringBuilder sb = new StringBuilder("SPECTATOR REPORT - " + start.toString() + " to " + end.toString() + "\n");
                sb.append("FILM NAME | NUMBER OF SPECTATORS | MEAN RATING\n");
                for (Film film : filmList) {
                    sb.append(film.getName()).append(" | ").append(film.getNumSpectators(start, end)).append(" | ").append(String.format("%.2f", film.getRating(start, end))).append("\n");
                }
                return sb.toString();
            }else{
                throw new IllegalArgumentException("Start date can't be after end date!");
            }
        } else{
            throw new NoPermissionException(permExceptionString(perm));
        }
    }

    @Override
    public String runGrossReport(LocalDate start, LocalDate end) throws NoPermissionException, IllegalArgumentException {
        Permissions perm = Permissions.RUN_REPORTS;
        if (validatePermission(perm)) {
            if (end.isAfter(start.minusDays(1))){
                MapToCollection<String, Film> m2c = new MapToCollection<>(this.films);
                List<Film> filmList = m2c.getSortedList(Comparator.comparingInt(o -> o.getTakings(start, end)));
                StringBuilder sb = new StringBuilder("INCOME REPORT - " + start.toString() + " to " + end.toString() + "\n");
                StringBuilder sb1 = new StringBuilder();
                int totalTakings = 0;
                for (Film film : filmList) {
                    totalTakings = totalTakings + film.getTakings(start, end);
                    sb1.append(film.getName()).append(": ").append(toDecimalCurrency(film.getTakings(start, end), "£")).append("\n");
                }
                sb.append("TOTAL TAKINGS: ").append(toDecimalCurrency(totalTakings, "£")).append("\n");
                sb.append(sb1.toString());
                return sb.toString();
            }else{
                throw new IllegalArgumentException("Start date can't be after end date!");
            }
        } else{
            throw new NoPermissionException(permExceptionString(perm));
        }
    }

    @Override
    public User getAuthenticatedUser(){
        return authenticatedUser;
    }

    @Override
    public int getPrice(Showing show, String seatType) throws IllegalArgumentException {
        int uplift;
        int base;
        if (show == null) {
            throw new IllegalArgumentException("The showing does not exist!");
        } else {
            base = show.getCost();
            switch (seatType) {
                case "VIP":
                    uplift = new VIPSeat().getCostUplift();
                    break;
                case "Standard":
                    uplift = new StandardSeat().getCostUplift();
                    break;
                default:
                    throw new IllegalArgumentException("Invalid Seat Type");
            }
        }
        return base + uplift;
    }

    @Override
    public Film getFilm(String exactName) throws IllegalArgumentException{
        Film film = films.get(exactName);
        if (film == null) {
            throw new IllegalArgumentException("Film does not exist");
        }else{
            return film;
        }
    }

    @Override
    public Set<Showing> getShowings(String filmName, LocalDate date) throws IllegalArgumentException{
        Film film = films.get(filmName);
        if (film == null) {
            throw new IllegalArgumentException("Film does not exist");
        }
        return film.getShowings(date);
    }

    @Override
    public Set<Showing> getShowings(LocalDate date){
        Set<Showing> retShowings = new HashSet<>();
        for (Screen screen : screens){
            retShowings.addAll(screen.getShowings(date));
        }
        return retShowings;
    }

    @Override
    public String toDecimalCurrency(int money, String symbol){
        int bigMoney = money / 100;
        int littleMoney = money % 100;
        String littleMoneyString = String.format("%02d", littleMoney);
        return symbol + bigMoney + "." + littleMoneyString;
    }

    private boolean validatePermission(Permissions p){
            return authenticatedUser != null && this.authenticatedUser.getPermissions().contains(p);
    }

    private String permExceptionString(Permissions p){
        return "Sorry, you do not have the " + p.name() + " Permission";
    }

    private boolean showingClash(Set<Showing> showings, LocalDateTime time, Film film){
        LocalDateTime filmEndTime = time.plusMinutes(film.getDuration().toMinutes());
        return (showings.parallelStream().anyMatch(o->{
            LocalDateTime startTime = o.getDatetime();
            LocalDateTime endTime = o.getDatetime().plus(o.getFilm().getDuration());
            return ((time.isAfter(startTime) && time.isBefore(endTime)) || (filmEndTime.isAfter(startTime) && filmEndTime.isBefore(endTime)));
        }
        ));
    }

    private void doAddBooking(Showing showing, Seat seat, Customer customer, Payment payment) throws BookingFailedException{
        if (seat == null){
            throw new BookingFailedException("No seats available!");
        }else if (showing == null) {
            throw new BookingFailedException("The showing does not exist!");
        } else if (payment == null) {
            throw new BookingFailedException("The payment does not exist!");
        }else if (customer == null){
            throw new BookingFailedException("The customer does not exist!");
        }else {
            if (payment.wasSuccessful()){
                //Booking(Customer customer, Showing showing, Payment payment, Seat seat) {
                Booking booking = new Booking(customer, showing, payment, seat);
                showing.addBooking(booking);
                bookings.add(booking);
                customer.addBooking(booking);
            } else{
                throw new BookingFailedException("Payment was not successful");
            }
        }
    }
}
