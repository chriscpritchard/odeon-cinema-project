/**
 * The CollectionTools Package - provides methods to convert from one collection type to another (as well as similar tools for maps)
 * This code is from https://github.com/chriscpritchard/6WCM0001/tree/customer-complaints-system/customer-complaints-system-assignment/src/CollectionTools
 * And has been used as part of the 6WCM0001 module outside of the assignment
 * In addition to the author of both the CollectionTools package and the odeoncinemaproject package being the same
 * This code is Licensed under the MIT license (https://github.com/chriscpritchard/6WCM0001/blob/customer-complaints-system/LICENSE)
 * @author Chris Pritchard
 * @version 2017-11-10
 */
package uk.co.christopherpritchard.CollectionTools;

